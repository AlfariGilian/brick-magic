//
//  ResponsePointHistory.swift
//  Brik Magic
//
//  Created by Alfari on 25/06/21.
//

import Foundation
import SwiftyUserDefaults

struct ResponsePointHistory: Codable, DefaultsSerializable {
    
    var total_point: String!
    var trans_detail: [ResponseDetailTrans]!
    
    enum CodingKeys: String, CodingKey {
        case total_point
        case trans_detail
    }
}

//
//  BasePointHistory.swift
//  Brik Magic
//
//  Created by Alfari on 25/06/21.
//

import Foundation
import SwiftyUserDefaults


struct BasePointHistory: Codable, DefaultsSerializable {
    
    var status: String!
    var message: String!
    var code: Int!
    var data: ResponsePointHistory!
    
    enum CodingKeys: String, CodingKey {
        case status
        case message
        case code
        case data
    }
}

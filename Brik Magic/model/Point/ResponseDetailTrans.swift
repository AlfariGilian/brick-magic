//
//  ResponseDetailTrans.swift
//  Brik Magic
//
//  Created by Alfari on 25/06/21.
//

import Foundation
import SwiftyUserDefaults

struct ResponseDetailTrans: Codable, DefaultsSerializable {
    
    var AMOUNT: String!
    var TRANS_DATE: String!
    var REF_NO: String!
    var MEMBER_ID: String!
    var REMARKS: String!
    var POINT: String!
    var TYPE: String!
    var DISPLAY_ID: String!
    var NAME: String!
    
    enum CodingKeys: String, CodingKey {
        case AMOUNT
        case TRANS_DATE
        case REF_NO
        case MEMBER_ID
        case REMARKS
        case POINT
        case TYPE
        case DISPLAY_ID
        case NAME
    }
}

//
//  GenderModel.swift
//  Brik Magic
//
//  Created by Alfari on 22/06/21.
//

import Foundation
import SwiftyUserDefaults

struct GenderModel: Codable, DefaultsSerializable {
    var value: String!
    var label: String!
    
    enum CodingKeys: String, CodingKey {
         case value = "value"
         case label = "label"
    }
}

//
//  ResponseGetNews.swift
//  Brik Magic
//
//  Created by Alfari on 24/06/21.
//

import Foundation
import SwiftyUserDefaults

struct ResponseGetNews: Codable, DefaultsSerializable {
    
    var REC_ID: String!
    var IMAGE: String!
    var SUB_TITLE: String!
    var TITLE: String!
    
    enum CodingKeys: String, CodingKey {
        case REC_ID
        case IMAGE
        case SUB_TITLE
        case TITLE
    }
}


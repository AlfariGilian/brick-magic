//
//  BaseNews.swift
//  Brik Magic
//
//  Created by Alfari on 24/06/21.
//

import Foundation
import SwiftyUserDefaults


struct BaseNews: Codable, DefaultsSerializable {
    
    var status: String!
    var message: String!
    var code: Int!
    var data: [ResponseGetNews]!
    
    enum CodingKeys: String, CodingKey {
        case status
        case message
        case code
        case data
    }
}

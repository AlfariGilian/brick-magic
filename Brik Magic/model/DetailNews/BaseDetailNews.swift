//
//  BaseDetailNews.swift
//  Brik Magic
//
//  Created by Alfari on 25/06/21.
//

import Foundation
import SwiftyUserDefaults


struct BaseDetailNews: Codable, DefaultsSerializable {
    
    var status: String!
    var message: String!
    var code: Int!
    var data: [ResponseDetailNews]!
    
    enum CodingKeys: String, CodingKey {
        case status
        case message
        case code
        case data
    }
}

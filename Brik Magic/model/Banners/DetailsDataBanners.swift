//
//  DetailsDataBanners.swift
//  Brik Magic
//
//  Created by Alfari on 07/07/21.
//

import Foundation
import SwiftyUserDefaults


struct DetailsDataBanners: Codable, DefaultsSerializable {
    
    var DATE_START: String!
    var DATE_END: String!
    var URL: String!
    var PATH: String!
    
    enum CodingKeys: String, CodingKey {
        case DATE_START = "DATE_START"
        case DATE_END = "DATE_END"
        case URL = "URL"
        case PATH = "PATH"
    }
}

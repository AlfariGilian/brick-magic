//
//  BaseDataBanners.swift
//  Brik Magic
//
//  Created by Alfari on 07/07/21.
//

import Foundation
import SwiftyUserDefaults


struct BaseDataBanners: Codable, DefaultsSerializable {
    
    var status: String!
    var message: String!
    var code: Int!
    var data: [DetailsDataBanners]!
    
    enum CodingKeys: String, CodingKey {
        case status
        case message
        case code
        case data
    }
}


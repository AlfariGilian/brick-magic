//
//  ResponseGetStore.swift
//  Brik Magic
//
//  Created by Alfari on 24/06/21.
//

import Foundation
import SwiftyUserDefaults

struct ResponseGetStore: Codable, DefaultsSerializable {
    
    var NAME: String!
    var ADDRESS: String!
    var PHONE_NUMBER: String!
    var LONGITUDE: String!
    var LATITUDE: String!

    
    enum CodingKeys: String, CodingKey {
        case NAME
        case ADDRESS
        case PHONE_NUMBER
        case LONGITUDE
        case LATITUDE
    }
}

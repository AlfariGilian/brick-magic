//
//  BaseStore.swift
//  Brik Magic
//
//  Created by Alfari on 24/06/21.
//

import Foundation
import SwiftyUserDefaults


struct BaseStore: Codable, DefaultsSerializable {
    
    var status: String!
    var message: String!
    var code: Int!
    var data: [ResponseGetStore]!
    
    enum CodingKeys: String, CodingKey {
        case status
        case message
        case code
        case data
    }
}

//
//  BaseDataLogin.swift
//  Brik Magic
//
//  Created by Alfari on 21/06/21.
//

import Foundation
import SwiftyUserDefaults

struct BaseDataLogin: Codable, DefaultsSerializable {
    
    var status: String!
    var message: String!
    var code: Int!
    
    
    enum CodingKeys: String, CodingKey {
        case status
        case message
        case code
    }
  
}

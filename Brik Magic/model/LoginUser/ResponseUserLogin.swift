//
//  ResponseUserLogin.swift
//  Brik Magic
//
//  Created by Alfari on 21/06/21.
//

import Foundation
import SwiftyUserDefaults

struct ResponseUserLogin: Codable, DefaultsSerializable {
    
    var MEMBER_ID: String!
    var DISPLAY_ID: String!
    
    enum CodingKeys: String, CodingKey {
        case MEMBER_ID
        case DISPLAY_ID
    }
  
}

//
//  BaseResponseLoginUser.swift
//  Brik Magic
//
//  Created by Alfari on 21/06/21.
//

import Foundation
import SwiftyUserDefaults

struct BaseResponseUserLogin: Codable, DefaultsSerializable {
    
    var status: String!
    var message: String!
    var code: Int!
    var data: ResponseUserLogin!
    
    enum CodingKeys: String, CodingKey {
        case status
        case message
        case code
        case data
    }
}

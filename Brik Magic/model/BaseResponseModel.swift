//
//  BaseResponseModel.swift
//  Brik Magic
//
//  Created by Alfari on 17/05/21.
//

import Foundation
import SwiftyUserDefaults

struct BaseResponseModel<T: Codable>: Codable,  DefaultsSerializable {
    
    var message: String!
    var code: Int!
    var data: T!
    
    
    enum CodingKeys: String, CodingKey {
        case code = "code"
        case message = "message"
        case data
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.code     = try container.decode(Int.self, forKey: .code)
        self.message  = try container.decode(String.self, forKey: .message)
        self.data    = try container.decodeIfPresent(T.self, forKey: .data)
    }
    
    func encode(to encoder: Encoder) throws {
        do {
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(code, forKey: .code)
            try container.encode(message, forKey: .message)
            try container.encode(data, forKey: .data)
        } catch {
            #if DEBUG
            print(error)
            #endif
        }
    }
    
}

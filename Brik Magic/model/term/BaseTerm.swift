//
//  BaseTerm.swift
//  Brik Magic
//
//  Created by Alfari on 30/06/21.
//

import Foundation
import SwiftyUserDefaults


struct BaseTerm: Codable, DefaultsSerializable {
    var message: String!
    var code: Int!
    var data: TermDetails!
    
    enum CodingKeys: String, CodingKey {
        case message
        case code
        case data
    }
}

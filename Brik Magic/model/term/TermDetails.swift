//
//  TermDetails.swift
//  Brik Magic
//
//  Created by Alfari on 30/06/21.
//

import Foundation
import SwiftyUserDefaults


struct TermDetails: Codable, DefaultsSerializable {
    
    var url: String!
    
    enum CodingKeys: String, CodingKey {
        case url
    }
}

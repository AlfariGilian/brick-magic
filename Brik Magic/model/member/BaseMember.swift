//
//  BaseMember.swift
//  Brik Magic
//
//  Created by Alfari on 26/06/21.
//

import Foundation
import SwiftyUserDefaults


struct BaseMember: Codable, DefaultsSerializable {
    
    var message: String!
    var code: Int!
    var data: DataMemberDetails!
    
    enum CodingKeys: String, CodingKey {
        case message
        case code
        case data
    }
}

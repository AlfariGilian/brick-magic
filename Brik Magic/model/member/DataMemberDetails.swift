//
//  DataMemberDetails.swift
//  Brik Magic
//
//  Created by Alfari on 26/06/21.
//

import Foundation
import SwiftyUserDefaults

struct DataMemberDetails: Codable, DefaultsSerializable {
    
    var FIRST_NAME: String!
    var LAST_NAME: String!
    var MOBILE_NO: String!
    var EMAIL: String!
    var ID_CARD_NO: String!
    var BIRTH_DATE: String!
    var GENDER: String!
    var MEMBER_ID: String!
    var JOIN_DATE: String!
    var VALID_UNTIL: String!
    var BRICK_TOTAL: String!
    var TRANSACTION_TOTAL: String!
    var DISPLAY_ID: String!
//    var LAST_LOGIN: String!
    var VIB_MEMBER: Bool = true
//    var CURRENT_AMOUNT: String!
    var UPGRADE: Bool = true
    
    enum CodingKeys: String, CodingKey {
        case FIRST_NAME = "FIRST_NAME"
        case LAST_NAME = "LAST_NAME"
        case MOBILE_NO = "MOBILE_NO"
        case EMAIL = "EMAIL"
        case ID_CARD_NO = "ID_CARD_NO"
        case BIRTH_DATE = "BIRTH_DATE"
        case GENDER = "GENDER"
        case MEMBER_ID = "MEMBER_ID"
        case JOIN_DATE = "JOIN_DATE"
        case VALID_UNTIL = "VALID_UNTIL"
        case BRICK_TOTAL = "BRICK_TOTAL"
        case TRANSACTION_TOTAL = "TRANSACTION_TOTAL"
        case DISPLAY_ID = "DISPLAY_ID"
//        case LAST_LOGIN = "LAST_LOGIN"
        case VIB_MEMBER = "VIB_MEMBER"
//        case CURRENT_AMOUNT = "CURRENT_AMOUNT"
        case UPGRADE = "UPGRADE"
    }
}

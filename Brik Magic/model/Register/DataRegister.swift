//
//  DataRegister.swift
//  Brik Magic
//
//  Created by Alfari on 21/07/21.
//

import Foundation
import SwiftyUserDefaults

struct DataRegister: Codable, DefaultsSerializable {
    
    var member_id: String!
    var display_id: String!
    
    enum CodingKeys: String, CodingKey {
        case member_id = "member_id"
        case display_id = "display_id"
    }
  
}

//
//  BaseRegister.swift
//  Brik Magic
//
//  Created by Alfari on 21/07/21.
//

import Foundation
import SwiftyUserDefaults

struct BaseRegister: Codable, DefaultsSerializable {
    
    var status: String!
    var message: String!
    var code: Int!
    var data: DataRegister!
    
    enum CodingKeys: String, CodingKey {
        case status
        case message
        case code
        case data
    }
}

//
//  BackgroundDetails.swift
//  Brik Magic
//
//  Created by Alfari on 26/06/21.
//

import Foundation
import SwiftyUserDefaults


struct BackgroundDetails: Codable, DefaultsSerializable {
    
    var PATH: String!
    
    enum CodingKeys: String, CodingKey {
        case PATH
    }
}

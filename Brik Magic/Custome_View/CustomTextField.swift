//
//  CustomTextField.swift
//  Brik Magic
//
//  Created by Alfari on 30/06/21.
//

import UIKit
import Foundation

class CustomTextField: UITextField {
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var cornerRadius: CGFloat = 5
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.setOutlineRoundedStyle()
    }
    required override init(frame: CGRect) {
        super.init(frame: frame)
        self.setOutlineRoundedStyle()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func setOutlineRoundedStyle(){
        setOutlineRoundedStyle(color: ColorController.outlineColorDefault()!, width: borderWidth, rounded:cornerRadius)
    }
    
    func setOutlineRoundedStyle(color:UIColor) {
        setOutlineRoundedStyle(color: color, width: borderWidth, rounded:cornerRadius)
    }
    
    func setOutlineRoundedStyle(width:CGFloat) {
        setOutlineRoundedStyle(color: ColorController.outlineColorDefault()!, width: width, rounded:cornerRadius)
    }
    
    func setOutlineRoundedStyle(color:UIColor, width:CGFloat, rounded:CGFloat) {
        layer.borderColor = color.cgColor
        layer.borderWidth = width
//        layer.backgroundColor = UIColor.white.cgColor
        setRounded(rounded: rounded)
    }
    
    func setRounded(){
        setRounded(rounded: cornerRadius)
    }
    
    func setRounded(rounded:CGFloat) {
        cornerRadius = rounded
        layer.cornerRadius = cornerRadius
    }
    
    func getPadding(plusExtraFor clearButtonMode: ViewMode) -> UIEdgeInsets {
        var padding = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        // Add additional padding on the right side when showing the clear button
        if self.clearButtonMode == .always || self.clearButtonMode == clearButtonMode {
            padding.right = 28
        }
        return padding
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        let padding = getPadding(plusExtraFor: .unlessEditing)
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        let padding = getPadding(plusExtraFor: .unlessEditing)
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        let padding = getPadding(plusExtraFor: .whileEditing)
        return bounds.inset(by: padding)
    }
    
        
        @IBInspectable var paddingLeftCustom: CGFloat {
            get {
                return leftView!.frame.size.width
            }
            set {
                let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
                leftView = paddingView
                leftViewMode = .always
            }
        }
        
        @IBInspectable var paddingRightCustom: CGFloat {
            get {
                return rightView!.frame.size.width
            }
            set {
                let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
                rightView = paddingView
                rightViewMode = .always
            }
        }
        
    
}

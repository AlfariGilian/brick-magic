//
//  ScreenSizeUtil.swift
//  Brik Magic
//
//  Created by spe on 4/13/21.
//

import UIKit

class ScreenSizeUtil {
    
    static let HEIGHT_SCREEN = UIScreen.main.bounds.size.height
    static let WIDTH_SCREEN = UIScreen.main.bounds.size.width
    
    static func screenSize() -> CGFloat{
        switch WIDTH_SCREEN {
        case 320: // SE, 5s
            return 100
        case 375: //6, 6s, 7, 8, X, XS
            return 120
        case 414: //6s plus, 7 plus, 8 plus, XSmax, XR
            return 150
        default:
            return 100
        }
    }
    
    static func constantTopTab() -> CGFloat{
        switch WIDTH_SCREEN {
        case 320: // SE, 5s
            return 0
        case 375: //6, 6s, 7, 8, X, XS
            return 20
        case 414: //6s plus, 7 plus, 8 plus, XSmax, XR
            return 20
        default:
            return 20
        }
    }
}

class ScreenHomeUtil {
    
    static let HEIGHT_SCREEN = UIScreen.main.bounds.size.height
    static let WIDTH_SCREEN = UIScreen.main.bounds.size.width
    
    static func screenSize() -> Int{
        switch WIDTH_SCREEN {
        case 320: // SE, 5s
            return 5
        case 375: //6, 6s, 7, 8, X, XS
            return 6
        case 414: //6s plus, 7 plus, 8 plus, XSmax, XR
            return 7
        default:
            return 5
        }
    }
}

//
//  Tags.swift
//  Brik Magic
//
//  Created by Alfari on 17/05/21.
//

import Foundation

class Tags: Constant {
    //for int data
    static let PAGE_SIZE_TOTAL = 20
    
    //for string data
    static let UUID = "uuid"
    static let START_DATE = "start_date"
    static let END_DATE = "end_date"
    static let GROUP = "group"
    static let GATE_LOCATION = "gate_location"
    
}

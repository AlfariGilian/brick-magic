//
//  DefaultsKeys.swift
//  Brik Magic
//
//  Created by Alfari on 24/06/21.
//

import Foundation
import SwiftyUserDefaults

extension DefaultsKeys {
    
    static let responseUserLogin = DefaultsKey<ResponseUserLogin?>("responseUserLogin")
    
    static let responseUserRegister = DefaultsKey<DataRegister?>("responseUserRegister")
    
    static let dataMemberDetail = DefaultsKey<DataMemberDetails?>("dataMemberDetails")
    
    static let dataBaseMemberDetail = DefaultsKey<DataMemberDetails?>("dataBaseMemberDetails")
    
    static let bgCard = DefaultsKey<BackgroundDetails?>("detailsCard")
}

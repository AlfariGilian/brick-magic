//
//  ScaleUI.swift
//  Brik Magic
//
//  Created by Alfari on 17/05/21.
//

import Foundation
import UIKit


class ScaleUI {
    
    func detectSize(def: CGFloat) -> CGFloat {
        return def * calculatePoint()
    }
    
    func calculatePoint() -> CGFloat {
       
        let curHeight = UIScreen.main.bounds.height
        let curWidth = UIScreen.main.bounds.width
        let diagonal = sqrt(pow(curHeight, 2) + pow(curWidth, 2))
        
        let baseHeight: CGFloat = 812
        let baseWidth: CGFloat = 375
        let baseDiagonal = sqrt(pow(baseHeight, 2) + pow(baseWidth, 2))
        
        // "14" font size is defult font size
        let percentage = (diagonal / baseDiagonal)
        
        return percentage
    }
}


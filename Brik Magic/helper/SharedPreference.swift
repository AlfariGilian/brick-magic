//
//  SharedPreference.swift
//  Brik Magic
//
//  Created by Alfari on 26/06/21.
//

import Foundation
import SwiftyJSON


class SharedPreference {
    
    static let FLAG_IS_PIN = "Flag Is Pin"
    
    static func save(value: Any?, tag: String) {
        let defaults = UserDefaults.standard
        defaults.setValue(value, forKey: tag)
        defaults.synchronize()
    }
    
    static func s(value: Any?, tag: String) {
        let defaults = UserDefaults.standard
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: value!)
        
        defaults.set(encodedData, forKey: tag)
        defaults.synchronize()
    }
    
    static func g(tag: String) -> Any? {
        let defaults = UserDefaults.standard
        if let data = defaults.object(forKey: tag) as? Data {
            let decoded = NSKeyedUnarchiver.unarchiveObject(with: data)
            return decoded
        }
        return nil
    }
    
    static func r(tag: String) {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: tag)
    }
    
    static func remove(tag: String) {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: tag)
    }
    
    static func get(tag: String) -> Any? {
        let defaults = UserDefaults.standard
        return defaults.object(forKey: tag)
    }
    
    
}

//
//  ColorController.swift
//  Brik Magic
//
//  Created by spe on 4/13/21.
//

import Foundation
import UIKit

class ColorController {
    
    class func borderColor() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "d3d3d3")
        return color
    }
   
    class func textColor() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "444444")
        return color
    }
    
    class func lineBersama() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "EEEEEE")
        return color
    }
    
    class func yellowBersama() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "E3BB38")
        return color
    }
    
    class func blueBersama1() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "1070aa")
        return color
    }
    
    class func blueBersama2() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "0f699f")
        return color
    }
    
    class func blueBersama3() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "0dd557")
        return color
    }
    
    class func blueBersama4() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "7EB5D0")
        return color
    }
    
    class func blueBersama5() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "4092C1")
        return color
    }
    
    class func pinEmpty() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "EEEEEE")
        return color
    }
    
    class func shadow()-> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "00000029")
        return color
    }
    
    class func pinEntered() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "6E6E6E")
        return color
    }
    
    class func textHint() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "FFDB81")
        return color
    }
    
    class func textHint2() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "B7B7B7")
        return color
    }
    
    class func textHint3() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "6B6B6B")
        return color
    }
    
    class func greenBersama() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "00CC3A")
        return color
    }
    
    class func activeButton() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "2AB5AD")
        return color
    }
    
    class func chartSeries1() -> UIColor? {
         let color: UIColor? = colorWithHexString(withHexString: "FDB913")
         return color
     }
    
    class func chartSeries2() -> UIColor? {
         let color: UIColor? = colorWithHexString(withHexString: "FEE3A0")
         return color
     }
    
    class func orangeBersama1() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "FDB913")
        return color
    }
    
    class func orangeBersama2() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "F2AB1E")
        return color
    }
    
    class func redBersama() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "FF0000")
        return color
    }
    
    class func kikikukuBaseColor() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "61FFC2")
        return color
    }
    
    class func kikikukuBaseColor2() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "FFCC08")
        return color
    }
    
    class func borderBersama() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "EEEEEE")
        return color
    }
   
    class func grayBersama() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "B7B7B7")
        return color
    }
    
    class func grayBersama2() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "6E6E6E")
        return color
    }
    
    class func grayBersama3() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "F6F6F6")
        return color
    }
    
    class func grayBersama4() -> UIColor? {
           let color: UIColor? = colorWithHexString(withHexString: "D3D3D3")
           return color
       }
    class func grayTextBersama() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "444444")
        return color
    }
    
    class func redSpe() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "ff5050")
        return color
    }
    
    class func blueSpe() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "0257a1")
        return color
    }
    class func blueSpe2() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "477ABE")
        return color
    }
    
    class func tabItemFontColorActive() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "0257a1")
        return color
    }
    
    class func tabItemFontColor() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "808080")
        return color
    }
    
    class func outlineColorDefault() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "8f8f8f")
        return color
    }
    
    class func redErrorColor() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "FA262F")
        return color
    }
    
    class func bgHometype1() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "3A3A3A")
        return color
    }
    
    class func bgHometype2() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "282828")
        return color
    }
    
    class func MDCTabBarTintColor() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "fa262f")
        return color
    }
    
    class func MDCTabBarBottomDividerColor() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "5f5f5f")
        return color
    }
    
    class func customGreenColor() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "#20B2AA")
        return color
    }
    
    class func greyColorBg() -> UIColor? {
           let color: UIColor? = colorWithHexString(withHexString: "#DADADA")
           return color
       }
    
    class func blueColorBtn() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "#006DB7")
        return color
    }
    
    class func yelowColorBtn() -> UIColor? {
        let color: UIColor? = colorWithHexString(withHexString: "#FF9000")
        return color
    }
    
    class func colorWithHexString (withHexString hex: String?) -> UIColor {
        let hexString: String = hex!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue = CGFloat(b) / 255.0
        return UIColor(red:red, green:green, blue:blue, alpha:1.0)
    }
    
    
    class func clearColor () -> UIColor {
     
        let red = CGFloat(0) / 255.0
        let green = CGFloat(0) / 255.0
        let blue = CGFloat(0) / 255.0
        return UIColor(red:red, green:green, blue:blue, alpha:0.1)
    }
    
}

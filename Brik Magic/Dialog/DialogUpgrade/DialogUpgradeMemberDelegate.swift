//
//  DialogUpgradeMemberDelegate.swift
//  Brik Magic
//
//  Created by Alfari on 09/07/21.
//

import Foundation

protocol DialogUpgradeMemberDelegate {
    
    func excuteError()
    
    func excuteDismissError()
    
    func excuteDashboard()
    
//    func onFailedMemberUpgrade(message: String)
//    
//    func successUpdagradeMember()
}

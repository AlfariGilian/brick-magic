//
//  DialogUpgradeMemberPresenter.swift
//  Brik Magic
//
//  Created by Alfari on 09/07/21.
//

import Foundation
import SwiftyUserDefaults

class DialogUpgradeMemberPresenter: BasePresenter<DialogUpgradeMemberDelegate>, BaseProtocol {
    
    func doUpgrade(){
        
        requestId = 0
        
        let responseUserLogin = Defaults[.responseUserLogin]
        
        formData = [
            Tags.ID : "\(responseUserLogin?.MEMBER_ID ?? "")"
        ]
        
        let method = ApiProvider.updateMember(data: formData)
        Api<BaseDataLogin>.baseRequest(requestId : requestId, method : method, viewProt: view2)
    }
    
    func onSuccessRequest(requestId: Int, rawdata: String) {
        switch requestId {
        case 0:
            
            let objectData = try? JSONDecoder().decode( BaseDataLogin.self, from: rawdata.data(using: .utf8)!)
            
//            if objectData?.message == "user already upgraded" {
//                view.successUpdagradeMember()
//            } else {
//                view.onFailedMemberUpgrade(message: (objectData?.message)!)
//            }
            
            break
            
        default:
            break
            
        }
        
    }
    
    func onFailedRequest(status: String, requestId: Int, message: String) {
//        switch requestId {
//        default:
//            view.onFailedMemberUpgrade(message: message)
//            break
//        }
    }
}

//
//  DialogUpgradeMember.swift
//  Brik Magic
//
//  Created by Alfari on 09/07/21.
//

import Foundation
import UIKit
import Localize_Swift

class DialogUpgradeMember: UIViewController {
    
    
    var dialogUpgradeMemberDelegate: DialogUpgradeMemberDelegate?
    var presenter: DialogUpgradeMemberPresenter!
    var dialogMessageUpdate: DialogMessageUpdate!
    var isDismiss = ""
    
    @IBOutlet weak var btnUpgrade: UIView!
    
    class func instance(isDismiss: String, viewDelegate: DialogUpgradeMemberDelegate)-> DialogUpgradeMember {
        let vc = DialogUpgradeMember.init(nibName: "DialogUpgradeMember", bundle: nil)
        vc.dialogUpgradeMemberDelegate = viewDelegate
        return vc
    }
    
    override func viewDidLoad() {
        //        presenter = DialogUpgradeMemberPresenter(view: self)
        //        presenter.initBase(view: presenter)
        
        let executeTap = UITapGestureRecognizer(target: self, action: #selector(exec))
        btnUpgrade.isUserInteractionEnabled = true
        btnUpgrade.addGestureRecognizer(executeTap)
    }
    
    func showAlertMessageUpdate(){
        dialogMessageUpdate = DialogMessageUpdate.instance(isDismiss: "dismiss", viewDelegate: self)
        dialogMessageUpdate.view.center = view.center
        addChild(dialogMessageUpdate)
        view.addSubview(dialogMessageUpdate.view)
        dialogMessageUpdate.didMove(toParent: self)
    }
    
    @objc func exec(){
        if(isDismiss == "dismiss"){
            dialogUpgradeMemberDelegate?.excuteDismissError()
        }else{
            dialogUpgradeMemberDelegate?.excuteError()
        }
    }
    
    @objc func remove(){
        self.willMove(toParent: nil)
        self.removeFromParent()
        self.view.removeFromSuperview()
        showAlertMessageUpdate()
    }
}


extension DialogUpgradeMember: DialogMemberUpdateDelegate {
    
    func extError() {
        dialogMessageUpdate.remove()
    }
    
    func extDismissError() {
        dialogMessageUpdate.remove()
    }
    
    func extDashboard() {
        dialogMessageUpdate.remove()
    }
}



//
//  DialogErrorMessage.swift
//  Brik Magic
//
//  Created by Alfari on 21/06/21.
//

import Foundation
import UIKit
import Localize_Swift

class DialogErrorMessage: UIViewController {
    
    @IBOutlet weak var lblInfo: UILabel!
    
    @IBOutlet weak var lblMessage: UILabel!
    
    @IBOutlet weak var lblOk: UILabel!
    
    @IBOutlet weak var viewBtnOk: UIView!
    
    @IBOutlet weak var dialogHeight: NSLayoutConstraint!
    
    @IBOutlet weak var dialogWidth: NSLayoutConstraint!
    
    @IBOutlet weak var spaceHeight: NSLayoutConstraint!
    
    var titleMessage = ""
    var message = ""
    var isDismiss = ""
    var viewDelegate: DialogErrorDelegate?
    
    class func instance(title: String, message: String, isDismiss: String, viewDelegate: DialogErrorDelegate)-> DialogErrorMessage {
        let vc = DialogErrorMessage.init(nibName: "DialogErrorMessage", bundle: nil)
        
        vc.titleMessage = title
        vc.message = message
        vc.isDismiss = isDismiss
        vc.viewDelegate = viewDelegate
        
        return vc
    }
    
    func defaultSpaceWithoutToolbar(){
//        if(UIDevice.current.hasNotch){
//            self.spaceHeight.constant =  ScaleUI().detectSize(def: -44)
//        }else{
//            self.spaceHeight.constant =  ScaleUI().detectSize(def: 0)
//        }
    }
    
    
    override func viewDidLoad() {
//        lblInfo.font = ScaleFont().fontRegular(size:20)
//        lblMessage.font = ScaleFont().fontRegular(size:14)
//        lblOk.font = ScaleFont().fontRegular(size:14)
        
        viewBtnOk.layer.cornerRadius = 15
        
        defaultSpaceWithoutToolbar()
        
        let executeTap = UITapGestureRecognizer(target: self, action: #selector(exec))
        viewBtnOk.isUserInteractionEnabled = true
        viewBtnOk.addGestureRecognizer(executeTap)
        
//        let font = UIFont(name: "Cera-Pro-Bold", size: 13)

//        let labelTextSize = (message.localized()  as NSString).boundingRect(
//            with: CGSize(width: 310, height: CGFloat.greatestFiniteMagnitude),
//            options: .usesLineFragmentOrigin,
//            attributes: [.font: font!],
//            context: nil).size
        
//        dialogHeight.constant = CGFloat((labelTextSize.height + 130))
        
        lblMessage.text = message.localized()
        lblInfo.text = titleMessage
//        let width = UIScreen.main.bounds.width

//        switch width {
        
//        case 320:
//            dialogWidth.constant = CGFloat(280)
//            break
//        default:
//            dialogWidth.constant = CGFloat(320)
//        }
    }
    
    @objc func exec(){
        if(isDismiss == "dismiss"){
            viewDelegate?.executeDismissError()
        }else{
            viewDelegate?.executeError()
        }
    }
    
    @objc func remove(){
        self.willMove(toParent: nil)
        self.removeFromParent()
        self.view.removeFromSuperview()
    }
}


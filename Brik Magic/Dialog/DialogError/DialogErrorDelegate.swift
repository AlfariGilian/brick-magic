//
//  DialogErrorDelegate.swift
//  Brik Magic
//
//  Created by Alfari on 22/06/21.
//

import Foundation


protocol DialogErrorDelegate {
    func executeError()
    func executeDismissError()
    func executeDashboard()
}

//
//  LoadingScreenController.swift
//  Brik Magic
//
//  Created by Alfari on 17/05/21.
//

import Foundation
import NVActivityIndicatorView

 
class LoadingScreenController: UIViewController {
    
    @IBOutlet weak var loadingView: NVActivityIndicatorView!
    @IBOutlet weak var loadingHeight: NSLayoutConstraint!
    
    
    class func instance()-> LoadingScreenController {
        let vc = LoadingScreenController.init(nibName: "LoadingScreenController", bundle: nil)
        
        return vc
    }
    
    override func viewDidLoad() {
        loadingHeight.constant = ScaleUI().detectSize(def: 100)
    }
    
    @objc func updateLoading(){
        loadingView.startAnimating()
        
    }
    
    @objc func remove(){
        loadingView.stopAnimating()
        self.willMove(toParent: nil)
        self.removeFromParent()
        self.view.removeFromSuperview()
    }
    
    @objc func updateDone(message: String){
           DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
               self.remove()
           })
       }
}

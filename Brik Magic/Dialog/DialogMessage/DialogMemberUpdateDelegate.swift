//
//  DialogMemberUpdateDelegate.swift
//  Brik Magic
//
//  Created by Alfari on 13/07/21.
//

import Foundation

protocol DialogMemberUpdateDelegate {
    
    func extError()
    func extDismissError()
    func extDashboard()
}

//
//  DialogMessageUpdate.swift
//  Brik Magic
//
//  Created by Alfari on 13/07/21.
//

import Foundation
import UIKit

class DialogMessageUpdate: UIViewController {
    
    var dialogMemberUpgrade: DialogMemberUpdateDelegate?
    
    var isDismiss = ""
    
    @IBOutlet weak var imgClose: UIImageView!
    
    @IBOutlet weak var btnUpgrade: UIView!
    
    class func instance(isDismiss: String, viewDelegate: DialogMemberUpdateDelegate)-> DialogMessageUpdate {
        let vc = DialogMessageUpdate.init(nibName: "DialogMessageUpdate", bundle: nil)
        vc.dialogMemberUpgrade = viewDelegate
        return vc
    }
    
    override func viewDidLoad() {
        
        let executeTap = UITapGestureRecognizer(target: self, action: #selector(exec))
        imgClose.isUserInteractionEnabled = true
        imgClose.addGestureRecognizer(executeTap)
    }
    
    @objc func exec(){
        if(isDismiss == "dismiss"){
            dialogMemberUpgrade?.extDismissError()
        }else{
            dialogMemberUpgrade?.extError()
        }
    }
    
    @objc func remove(){
        self.willMove(toParent: nil)
        self.removeFromParent()
        self.view.removeFromSuperview()
    }
    
}

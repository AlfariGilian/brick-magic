//
//  NewsBannerPager.swift
//  Brik Magic
//
//  Created by Alfari on 09/07/21.
//

import Foundation
import FSPagerView

class NewsBannerPager: FSPagerViewCell {
    
    @IBOutlet weak var lblNameBanner: UILabel!
    @IBOutlet weak var imgNewsBanner: UIImageView!
    @IBOutlet weak var lblDescBanner: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

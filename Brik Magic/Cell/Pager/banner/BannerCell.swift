//
//  BannerCell.swift
//  Brik Magic
//
//  Created by Alfari on 09/07/21.
//

import Foundation
import FSPagerView

class BannerCell: FSPagerViewCell {
    
    @IBOutlet weak var imgBanners: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

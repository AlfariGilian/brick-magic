//
//  NewsViewCell.swift
//  Brik Magic
//
//  Created by spe on 4/14/21.
//

import Foundation
import UIKit

class NewsViewCell: UITableViewCell {
    
    @IBOutlet weak var imgNews: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblDesc: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}

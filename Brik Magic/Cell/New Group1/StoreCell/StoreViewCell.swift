//
//  StoreViewCell.swift
//  Brik Magic
//
//  Created by spe on 4/14/21.
//

import Foundation
import UIKit

class StoreViewCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblAddress: UILabel!
    
    @IBOutlet weak var lblTelp: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
}

//
//  RegisterViewDelegate.swift
//  Brik Magic
//
//  Created by Alfari on 22/06/21.
//

import Foundation


protocol RegisterViewDelegate: BaseDelegate {
    
    func onSuccessRegister()
    
    func onFailedRequest(message: String)
    
    func onSuccessAddBg()
}

//
//  RegisterViewPresenter.swift
//  Brik Magic
//
//  Created by Alfari on 22/06/21.
//

import Foundation
import SwiftyUserDefaults


class RegisterViewPresenter : BasePresenter<RegisterViewDelegate>, BaseProtocol {
    
    
    func doRegisterUser(firstName: String, lastName: String, phoneNumber: String, email: String, idCard: String, dateOfBirth: String, password: String, coPass: String, gender: String) {
        
        formData = [
            Tags.FIRST_NAME : firstName,
            Tags.LAST_NAME : lastName,
            Tags.PHONE_NUMBER : phoneNumber,
            Tags.EMAIL : email,
            Tags.ID_CARD : idCard,
            Tags.DATE_OF_BIRTH : dateOfBirth,
            Tags.PASSWORD : password,
            Tags.COMFIRM_PASSWORD : coPass,
            Tags.GENDER : gender
        ]
        
        let method = ApiProvider.regist(data: formData)
        requestId = 0
        Api<BaseRegister>.baseRequest(requestId : requestId, method : method, viewProt: self)
    }
    
    func doMemberDetail() {
        let responseUserLogin = Defaults[.responseUserRegister]
        
        formData = [
            Tags.ID : "\(responseUserLogin?.member_id ?? "")"
        ]
        
        method = ApiProvider.getMemberDetail(data: formData)
        requestId = 2
        Api<BaseMember>.baseRequest(requestId : requestId, method : method, viewProt: view2)
    }
    
    func doMemberCardBg() {
        formData = [:]
        method = ApiProvider.getMemberCard(data: formData)
        requestId = 1
        Api<BaseBackground>.baseRequest(requestId : requestId, method : method, viewProt: view2)
    }
    
    
    func onSuccessRequest(requestId: Int, rawdata: String) {
        switch requestId {
        case 0:
            let objectData = try? JSONDecoder().decode( BaseRegister.self, from: rawdata.data(using: .utf8)!)
            
            if objectData?.message == "success" {
                Defaults[.responseUserRegister] = objectData?.data
                if(Defaults[.responseUserRegister] != nil){
                    doMemberDetail()
                }
            } else {
                view.onFailedRequest(message: (objectData?.message)!)
            }
            break
            
        case 1:
            let objectData2 = try? JSONDecoder().decode( BaseBackground.self, from: rawdata.data(using: .utf8)!)
            Defaults[.bgCard] = objectData2?.data
            if Defaults[.bgCard] != nil {
                view.onSuccessAddBg()
            }
            
            break
            
        case 2:
            let objectData1 = try? JSONDecoder().decode(BaseMember.self, from: rawdata.data(using: .utf8)!)
            Defaults[.dataBaseMemberDetail] = objectData1?.data
            if(Defaults[.dataBaseMemberDetail] != nil){
                view.onSuccessRegister()
            } else {
                view.onFailedRequest(message: "Failed")
            }
            
            break
            
        default:
            break
            
        }
    }
    
    func onFailedRequest(status: String, requestId: Int, message: String) {
        switch requestId {
        default:
            view.onFailedRequest(message: message)
            break
        }
    }
}

//
//  RegisterViewController.swift
//  Brik Magic
//
//  Created by spe on 4/6/21.
//

import Foundation
import UIKit
import SwiftyUserDefaults
import IQKeyboardManagerSwift

class RegisterViewController: BaseViewController<RegisterViewPresenter> {
    
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var scrollRegister: UIScrollView!
    
    @IBOutlet weak var viewFirstname: UIView!
    @IBOutlet weak var viewLastName: UIView!
    @IBOutlet weak var viewPhoneNumber: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewIcNo: UIView!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var viewCoPassword: UIView!
    @IBOutlet weak var viewSelectGender: UIView!
    @IBOutlet weak var viewDateofBirth: UIView!
    
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var birthDateTf: UITextField!
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtIcCard: UITextField!
    @IBOutlet weak var txtDateOfBirth: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtComfirmPassword: UITextField!
    @IBOutlet weak var txtGender: UITextField!
    
    @IBOutlet weak var squareIv: UIImageView!
    @IBOutlet weak var checkIv: UIImageView!
    
    
    let datePicker = UIDatePicker()
    let datePickerView = UIPickerView()
    var mobilePhone = ""
    let genderList = ["Male", "Female"]
    var selectedGender: String?
    var isChecked = false
    
    class func instance(mobilePhone: String)->RegisterViewController {
        let vc = RegisterViewController.init(nibName: "RegisterViewController", bundle: nil)
        
        vc.mobilePhone = mobilePhone
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = RegisterViewPresenter(view: self)
        presenter.initBase(view: presenter)
        presenter.doMemberCardBg()
        
        IQKeyboardManager.shared.shouldResignOnTouchOutside = false
        
        let backgroundImage = UIImageView(frame: self.viewBg.bounds)
        backgroundImage.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        backgroundImage.image = UIImage(named: "bg_frame.png")
        backgroundImage.backgroundColor = UIColor.clear
        self.viewBg.insertSubview(backgroundImage, at: 0)
        
        btnRegister.layer.cornerRadius = 10
        viewFirstname.layer.cornerRadius = 10
        viewLastName.layer.cornerRadius = 10
        viewPhoneNumber.layer.cornerRadius = 10
        viewEmail.layer.cornerRadius = 10
        viewIcNo.layer.cornerRadius = 10
        viewPassword.layer.cornerRadius = 10
        viewCoPassword.layer.cornerRadius = 10
        viewSelectGender.layer.cornerRadius = 10
        viewDateofBirth.layer.cornerRadius = 10
        
        txtPhoneNumber.text = mobilePhone
        txtPhoneNumber.isEnabled = false
        checkIv.alpha = 0
        
        txtGender.text = "Male"
        txtFirstName.text = "Harry"
        txtLastName.text = "Style"
        txtEmail.text = "harry@gmail.com"
        txtIcCard.text = "9483483"
        txtDateOfBirth.text = "1989-10-19"
        txtPassword.text = "abc123"
        txtComfirmPassword.text = "abc123"
        
        datePickerView.delegate = self
        datePickerView.dataSource = self
        txtGender.inputView = datePickerView
        
        let checkedTap = UITapGestureRecognizer(target: self, action: #selector(checkedFunc))
        squareIv.isUserInteractionEnabled = true
        squareIv.addGestureRecognizer(checkedTap)
        
        showDatePicker()
        setupLbl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        scrollRegister.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 900)
    }
    
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        birthDateTf.inputAccessoryView = toolbar
        birthDateTf.inputView = datePicker
    }
    
    @objc func openDatePickerView(){
        txtGender.becomeFirstResponder()
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        birthDateTf.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    @objc func checkedFunc(){
        if(!isChecked){
            checkIv.alpha = 1
            isChecked = true
        } else {
            checkIv.alpha = 0
            isChecked = false
        }
    }
}

extension RegisterViewController {
    
    func setupLbl(){
        btnRegister.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doClickRegisterUser)))
    }
}

extension RegisterViewController {
    
    @objc func doClickRegisterUser(){
        
        if txtFirstName.text!.isEmpty{
            showAlertError(info: "Error", message: "Please fill in Your first name.".localized())
            return
        }
        
        if txtLastName.text!.isEmpty{
            showAlertError(info: "Error",message: "Please fill in Your last name.".localized())
            return
        }
        
        if txtEmail.text!.isEmpty{
            showAlertError(info: "Error",message: "Please fill in Your email address.".localized())
            return
        }
        
        if txtIcCard.text!.isEmpty{
            showAlertError(info: "Error",message: "Please fill in Your id card.".localized())
            return
        }
        if txtDateOfBirth.text!.isEmpty{
            showAlertError(info: "Error",message: "Please fill in Your date of birth.".localized())
            return
        }
        
        if txtGender.text!.isEmpty{
            showAlertError(info: "Error",message: "Please fill in Your gender.".localized())
            return
        }
        
        if txtPassword.text!.isEmpty{
            showAlertError(info: "Error",message: "Please fill in Your old password.".localized())
            return
        }
        
        if txtComfirmPassword.text!.isEmpty{
            showAlertError(info: "Error",message: "Please fill in Your confirmation password.".localized())
            return
        }
        
        if txtPassword.text!.localized() != txtComfirmPassword.text!.localized() {
            showAlertError(info: "Error",message: "Password doesn\'t match.".localized())
            return
        }
        
        if !isChecked {
            showAlertError(info: "Error",message: "Please tick and read the terms and conditions".localized())
            return
        }
        
        loading()
        presenter.doRegisterUser(firstName: txtFirstName.text!, lastName: txtLastName.text!, phoneNumber: txtPhoneNumber.text!, email: txtEmail.text!, idCard: txtIcCard.text!, dateOfBirth: txtDateOfBirth.text!, password: txtPassword.text!, coPass: txtComfirmPassword.text!, gender: txtGender.text!)
    }
    
    func updateData(){
        if(selectedGender == ""){
            //            heightToKewarganegaraan.constant = CGFloat(20)
            scrollRegister.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 1070)
        }else{
            //            heightToKewarganegaraan.constant = CGFloat(103)
            scrollRegister.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 1170)
        }
    }
}

extension RegisterViewController: RegisterViewDelegate {
    
    func onSuccessRegister() {
        removeLoading()
        let vc = DashboardViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func onFailedRequest(message: String) {
        removeLoading()
        showAlertError(info: "Error", message: message)
    }
    
    func onSuccessAddBg() {
        
    }
}

extension RegisterViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genderList.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = UILabel()
        if let v = view {
            label = v as! UILabel
        }
        label.text =  genderList[row]
        label.textAlignment = .center
        
        if selectedGender == "Male" {
            selectedGender = genderList[0]
            txtGender.text = selectedGender
        } else {
            selectedGender = genderList[1]
            txtGender.text = selectedGender
        }
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedGender = genderList[row]
        txtGender.text = selectedGender
        txtGender.textColor = UIColor.black
    }
}

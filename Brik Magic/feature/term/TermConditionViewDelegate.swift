//
//  TermConditionViewDelegate.swift
//  Brik Magic
//
//  Created by Alfari on 23/06/21.
//

import Foundation


protocol TermConditionViewDelegate: BaseDelegate {
    
    func onShowWebView(msg: String)
    
    func onFailedWebView(msg: String)
}

//
//  TermCondition.swift
//  Brik Magic
//
//  Created by spe on 4/6/21.
//

import Foundation
import UIKit


class TermConditionViewController : BaseViewController<TermConditionViewPresenter> {
    
    @IBOutlet weak var viewBackground: UIView!
    
    @IBOutlet var viewBg: UIView!
    
    @IBOutlet weak var imgBack: UIImageView!
    
    @IBOutlet weak var webViewContent: UIView!
    
    @IBOutlet weak var scrollView: FadedScrollView!
    
    @IBOutlet weak var lblWebView: UILabel!
    
    class func instance()->TermConditionViewController {
        let vc = TermConditionViewController.init(nibName: "TermConditionViewController", bundle: nil)
        return vc
    }
    
    override func viewDidLoad() {
        presenter = TermConditionViewPresenter(view: self)
        presenter.initBase(view: presenter)
        
        loading()
        presenter.getTerm()
        
        let backgroundImage = UIImageView(frame: self.viewBg.bounds)
        backgroundImage.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        backgroundImage.image = UIImage(named: "bg_halaman.png")
        backgroundImage.backgroundColor = UIColor.clear
        self.viewBg.insertSubview(backgroundImage, at: 0)
        
        let backgroundImage1 = UIImageView(frame: self.webViewContent.bounds)
        backgroundImage1.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        backgroundImage1.image = UIImage(named: "bg_transparant.png")
        backgroundImage1.backgroundColor = UIColor.clear
        self.webViewContent.insertSubview(backgroundImage1, at: 0)
        
        let backTap = UITapGestureRecognizer(target: self, action: #selector(dismissVc))
        imgBack.isUserInteractionEnabled = true
        imgBack.addGestureRecognizer(backTap)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 800)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    
    @objc func dismissVc(){
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
}

extension TermConditionViewController: TermConditionViewDelegate {
    
    func onShowWebView(msg: String) {
        removeLoading()
        
        let attrStr = try! NSAttributedString(
            data: msg.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
            options:[NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        lblWebView.attributedText = attrStr
    }
    
    func onFailedWebView(msg: String) {
        removeLoading()
    }
    
}

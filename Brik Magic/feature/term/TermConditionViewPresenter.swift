//
//  TermConditionViewPresenter.swift
//  Brik Magic
//
//  Created by Alfari on 23/06/21.
//

import Foundation
import SwiftyUserDefaults


class TermConditionViewPresenter: BasePresenter<TermConditionViewDelegate>, BaseProtocol {
    
    func getTerm() {
        requestId = 0
        formData = [:]
        
        let method = ApiProvider.getTerm(data: formData)
        Api<BaseTerm>.baseRequest(requestId : requestId, method : method, viewProt: self)
    }
    
    
    
    func onSuccessRequest(requestId: Int, rawdata: String) {
        switch requestId {
        case 0:
            let objectData = try? JSONDecoder().decode( BaseTerm.self, from: rawdata.data(using: .utf8)!)
            
            view.onShowWebView(msg: objectData!.data.url)
            break
            
        default:
            break
        }
    }
    
    func onFailedRequest(status: String, requestId: Int, message: String) {
        switch requestId {
        default:
            view.onFailedWebView(msg: message)
            break
        }
    }
}

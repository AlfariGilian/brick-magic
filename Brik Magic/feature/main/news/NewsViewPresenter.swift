//
//  NewsViewPresenter.swift
//  Brik Magic
//
//  Created by Alfari on 23/06/21.
//

import Foundation
import SwiftyUserDefaults

class NewsViewPresenter : BasePresenter<NewsViewDelegate>, BaseProtocol {
    
    func getDataNews() {
        requestId = 0
        formData = [:]
        let method = ApiProvider.getDataNews(data: formData)
        Api<BaseNews>.baseRequest(requestId : requestId, method : method, viewProt: self)
    }
    
    func onSuccessRequest(requestId: Int, rawdata: String) {
        switch requestId {
        case 0:
            let objectData = try? JSONDecoder().decode( BaseNews.self, from: rawdata.data(using: .utf8)!)
//            Defaults[.newsList] = objectData!.data
            view.onShowDataNews(newsList: objectData!.data)
            break
            
        default:
            break
        }
    }
    
    func onFailedRequest(status: String, requestId: Int, message: String) {
        switch requestId {
            
        default:
            view.onFailedGetData(msg: message)
            break
        }
    }
}

//
//  NewsViewDelegate.swift
//  Brik Magic
//
//  Created by Alfari on 23/06/21.
//

import Foundation

protocol NewsViewDelegate: BaseDelegate {
    func onShowDataNews(newsList: [ResponseGetNews])
    func onFailedGetData(msg: String)
}

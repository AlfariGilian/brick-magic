//
//  NewsViewController.swift
//  Brik Magic
//
//  Created by spe on 3/23/21.
//

import Foundation
import UIKit
import Localize_Swift
import SwiftyUserDefaults
import SDWebImage

class NewsViewController: BaseViewController<NewsViewPresenter> {
    
    @IBOutlet weak var newsTb: UITableView!
    
    var newsList : [ResponseGetNews] = []
    var delegate: DashboardViewDelegate!
    
    var loadingScreenViewController = LoadingScreenController.instance()
    var refreshControl = UIRefreshControl()
    var loadingData = false
    
    
    class func instance(delegate: DashboardViewDelegate)->NewsViewController {
        let vc = NewsViewController.init(nibName: "NewsViewController", bundle: nil)
        vc.delegate = delegate
        return vc
    }
    
    //    class func instance()->NewsViewController {
    //        let vc = NewsViewController.init(nibName: "NewsViewController", bundle: nil)
    //        return vc
    //    }
    
    @objc func didPullToRefresh() {
        getDataNews()
    }
    
    @objc func showLoading(){
        loadingScreenViewController.view.center = view.center
        addChild(loadingScreenViewController)
        self.view.addSubview(loadingScreenViewController.view)
        loadingScreenViewController.didMove(toParent: self)
    }
    
    override func viewDidLoad() {
        presenter = NewsViewPresenter(view: self)
        presenter.initBase(view: presenter)
        
        //        loading()
        //        presenter.getDataNews()
        
        refreshControl.tintColor = UIColor.black
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        
        
        newsTb.refreshControl = refreshControl
        refreshControl.isEnabled = false
        newsTb.dataSource = self
        newsTb.delegate = self
        newsTb.register(UINib(nibName: "NewsViewCell", bundle: nil), forCellReuseIdentifier: "newViewCell")
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .vertical
        newsTb.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 0, right: 0)
        
        getDataNews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    @objc func getDataNews(){
        getData()
    }
    
    func getData(){
        showLoading()
        presenter.getDataNews()
    }
}

extension NewsViewController: NewsViewDelegate {
    
    func onShowDataNews(newsList: [ResponseGetNews]) {
        loadingScreenViewController.remove()
        refreshControl.endRefreshing()
        refreshControl.isEnabled = true
        self.newsList = newsList
        newsTb.reloadData()
    }
    
    func onFailedGetData(msg: String) {
        refreshControl.endRefreshing()
        refreshControl.isEnabled = true
        loadingScreenViewController.remove()
    }
}

extension NewsViewController: UITableViewDelegate, UITableViewDataSource , UITextFieldDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: NewsViewCell = (tableView.dequeueReusableCell(withIdentifier: "newViewCell") as? NewsViewCell)!
        
        cell.lblTitle.text = newsList[indexPath.row].TITLE
        
        cell.imgNews.sd_setImage(with: URL(string: "http://103.6.53.214:8888/bricksmagic" + newsList[indexPath.row].IMAGE ), placeholderImage: UIImage(named: "ic_no_images"))
        cell.lblDesc.text =  newsList[indexPath.row].SUB_TITLE
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(218)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var idProduct = ""
        idProduct = newsList[indexPath.row].REC_ID
        let controller = DetailNewsViewController.instance(idProduct: idProduct)
        controller.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
}

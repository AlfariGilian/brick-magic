//
//  DashboardViewDelegate.swift
//  Brik Magic
//
//  Created by spe on 4/27/21.
//

import Foundation

protocol DashboardViewDelegate: BaseDelegate{
    func dismissVc()
    func resetAnimation()
}

//
//  DashboardViewController.swift
//  Brik Magic
//
//  Created by spe on 4/27/21.
//

import Foundation
import UIKit
import SwiftyUserDefaults
import CoreLocation


class DashboardViewController: UITabBarController, DashboardViewDelegate {
    
    
    let tabBarNormalImages = ["ic_home","ic_news","ic_scan_qr","ic_store","ic_account"]
    let tabBarSelectedImages = ["ic_home","ic_news","ic_scan_qr","ic_store","ic_account"]
    
    let tab = RootTabBar()
    var imageOffsetY: CGFloat = 0.0
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        UITableView.appearance().tableFooterView = UIView()
        UITableView.appearance().backgroundColor = ColorController.colorWithHexString(withHexString: "FFFFFF")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: ColorController.kikikukuBaseColor2()!], for:.selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.red], for:.normal)
        
        tab.tintColor = .white
        
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
            self.resetAnimation()
        })
        
        title = " "
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tab.invalidateIntrinsicContentSize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tab.isTranslucent = true
        tab.delegate = self
        tab.backgroundColor = .white
        tab.barTintColor = ColorController.kikikukuBaseColor2()!
        
        self.setValue(tab, forKey: "tabBar")
        self.setRootTabbarConntroller()
        
        if let tabBarItemsArray = tab.items {
            tabBarItemsArray[1].isEnabled = false
        }
        
    }
    
    func dismissVc() {
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
    
    func resetAnimation() {
        tab.layer.removeAllAnimations()
    }
}

class RootTabBar: UITabBar {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        var size = super.sizeThatFits(size)
        if #available(iOS 11.0, *) {
            let bottomInset = safeAreaInsets.bottom
            if bottomInset > 0 && size.height < 50 {
                size.height += bottomInset
            }
        }
        return size
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class RootNavigationController: UINavigationController{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.defaultSetting()
    }
    
    func defaultSetting(){
        self.navigationBar.barStyle = UIBarStyle.default
        self.navigationBar.barTintColor = ColorController.kikikukuBaseColor2()!
        self.navigationBar.isTranslucent = false
        
        self.navigationController?.navigationBar.titleTextAttributes =
            [NSAttributedString.Key.foregroundColor: ColorController.kikikukuBaseColor2()!,
             NSAttributedString.Key.font: UIFont.systemFont(ofSize: 11)]
        
        self.navigationController?.navigationBar.tintColor = ColorController.kikikukuBaseColor2()!
    }
    
    @objc func didBackButton(sender:UIButton){
        self.popViewController(animated:true)
    }
}

//MARK: Setup Function
extension DashboardViewController {
    
    func pushViewController(viewController: UIViewController){
        viewController.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func setRootTabbarConntroller(){
        var vc : UIViewController?
        for i in 0..<self.tabBarNormalImages.count {
            switch i {
            case 0:
                vc = HomeViewController.instance(delegate: self)
                
            case 1:
                vc = NewsViewController.instance(delegate: self)
                
            case 2:
                vc = GenerateQrViewController.instance()
                vc?.hidesBottomBarWhenPushed = true
                navigationController?.pushViewController(vc!, animated: true)
                
            case 3:
                vc = StoreViewController.instance(delegate: self)
                
            case 4:
                vc = ProfileViewController.instance(delegate: self)
                
            default:
                break
            }
            
            let nav = RootNavigationController.init(rootViewController: vc!)
            
            let barItem = UITabBarItem.init(title: "", image: UIImage.init(named: self.tabBarNormalImages[i])?.withRenderingMode(.alwaysOriginal), selectedImage: UIImage.init(named: self.tabBarSelectedImages[i])?.withRenderingMode(.alwaysOriginal))
            
            if(UIDevice.current.hasNotch){
                barItem.title = ""
                barItem.imageInsets = UIEdgeInsets(top: 100, left: 0, bottom: 120, right: 0)
            } else {
                barItem.title = ""
                barItem.imageInsets = UIEdgeInsets(top: 100, left: 0, bottom: 120, right: 0)
            }
            
            vc?.title = ""
            vc?.navigationController?.isNavigationBarHidden = true
            vc?.tabBarItem = barItem
            self.addChild(nav)
        }
    }
}


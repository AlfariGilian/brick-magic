//
//  ProfileViewController.swift
//  Brik Magic
//
//  Created by spe on 3/23/21.
//

import Foundation
import UIKit
import SwiftyUserDefaults

class ProfileViewController: BaseViewController<ProfileViewPresenter> {
    
    @IBOutlet weak var viewToolbar: UIView!
    
    @IBOutlet var viewBg: UIView!
    
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var viewChangePass: UIView!
    @IBOutlet weak var viewTerm: UIView!
    @IBOutlet weak var viewLogout: UIView!
    
    @IBOutlet weak var lblWelcome: UILabel!
    @IBOutlet var viewBackground: UIView!
    
    var memberDetails: DataMemberDetails!
    var delegate: DashboardViewDelegate!
    
    class func instance(delegate: DashboardViewDelegate)->ProfileViewController {
        let vc = ProfileViewController.init(nibName: "ProfileViewController", bundle: nil)
        vc.delegate = delegate
        return vc
    }
    
    override func viewDidLoad() {
        let backgroundImage = UIImageView(frame: self.viewBackground.bounds)
        backgroundImage.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        backgroundImage.image = UIImage(named: "bg_dasar.png")
        backgroundImage.backgroundColor = UIColor.clear
        self.viewBackground.insertSubview(backgroundImage, at: 0)
        
        memberDetails = Defaults[.dataBaseMemberDetail]
        if Defaults[.dataBaseMemberDetail] != nil {
            lblWelcome.text = "Welcome, " + memberDetails.FIRST_NAME
        } else {
            print("Kosong Cuk")
        }
        
        
        setupLbl()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
}

extension ProfileViewController {
    
    func setupLbl(){
        viewProfile.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doClickShowProfile)))
        
        viewChangePass.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doClickChangePass)))
        
        viewLogout.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doClickLogout)))
        
        viewTerm.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doClickTerm)))
    }
    
}

extension ProfileViewController {
    
    @objc func doClickShowProfile(){
        let vc = DetailProfileViewController.instance()
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func doClickChangePass(){
        let vc = ChangePasswordViewController.instance()
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func doClickLogout(){
        Defaults[.dataBaseMemberDetail] = nil
        let vc = LoginViewController.instance()
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func doClickTerm(){
        let vc = TermConditionViewController.instance()
        vc.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(vc, animated: true)
    }
    
}


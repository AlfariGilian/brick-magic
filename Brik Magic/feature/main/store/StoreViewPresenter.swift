//
//  StoreViewPresenter.swift
//  Brik Magic
//
//  Created by Alfari on 23/06/21.
//

import Foundation
import SwiftyUserDefaults

class StoreViewPresenter : BasePresenter<StoreViewDelegate>, BaseProtocol {
    
    func getDataStore(){
        requestId = 0
        formData = [:]
        let method = ApiProvider.getDataStore(data: formData)
        Api<BaseStore>.baseRequest(requestId : requestId, method : method, viewProt: self)
    }
    
    
    func onSuccessRequest(requestId: Int, rawdata: String) {
        switch requestId {
        case 0:
            let objectData = try? JSONDecoder().decode( BaseStore.self, from: rawdata.data(using: .utf8)!)
            view.onShowData(storeList: objectData!.data)
            break
            
        default:
            break
        }
    }
    
    func onFailedRequest(status: String, requestId: Int, message: String) {
        
        switch requestId {
            
        default:
            view.onFailed(msg: message)
            break
        }
        
    }
}

//
//  StoreViewController.swift
//  Brik Magic
//
//  Created by spe on 3/23/21.
//

import Foundation
import UIKit
import SwiftyUserDefaults


class StoreViewController : BaseViewController<StoreViewPresenter> {
    
    @IBOutlet weak var bgViewSearch: UIView!
    @IBOutlet weak var bgUtama: UIView!
    @IBOutlet weak var btnNearMe: UIView!
    
    @IBOutlet weak var tbStore: UITableView!
    @IBOutlet weak var txtStoreName: UITextField!
    
    @IBOutlet weak var bgTxt: UIView!
    
    var storeList: [ResponseGetStore] = []
    var delegate: DashboardViewDelegate!
    
    class func instance(delegate: DashboardViewDelegate)->StoreViewController {
        let vc = StoreViewController.init(nibName: "StoreViewController", bundle: nil)
        vc.delegate = delegate
        return vc
    }
    
    override func viewDidLoad() {
        
        presenter = StoreViewPresenter(view: self)
        presenter.initBase(view: presenter)
        loading()
        presenter.getDataStore()
        
        bgTxt.layer.cornerRadius = 10
        btnNearMe.layer.cornerRadius = 10
        
        let backgroundImage = UIImageView(frame: self.bgUtama.bounds)
        backgroundImage.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        backgroundImage.image = UIImage(named: "bg_dasar.png")
        backgroundImage.backgroundColor = UIColor.clear
        self.bgUtama.insertSubview(backgroundImage, at: 0)
        
        txtStoreName.setLeftPaddingPoints(15)
        
        loading()
        
        tbStore.dataSource = self
        tbStore.delegate = self
        tbStore.register(UINib(nibName: "StoreViewCell", bundle: nil), forCellReuseIdentifier: "tbStoreCell")
        
    }
    
}

extension StoreViewController: StoreViewDelegate {
    
    
    func onShowData(storeList: [ResponseGetStore]) {
        removeLoading()
        self.storeList = storeList
        tbStore.reloadData()
    }
    
    func onFailed(msg: String) {
        removeLoading()
    }
}

extension StoreViewController: UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return storeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: StoreViewCell = (tableView.dequeueReusableCell(withIdentifier: "tbStoreCell") as? StoreViewCell)!
        
        cell.lblTitle.text = storeList[indexPath.row].NAME
        cell.lblAddress.text = storeList[indexPath.row].ADDRESS
        cell.lblTelp.text =  storeList[indexPath.row].PHONE_NUMBER
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(100)
    }
}

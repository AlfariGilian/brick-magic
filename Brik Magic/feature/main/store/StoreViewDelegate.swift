//
//  StoreViewDelegate.swift
//  Brik Magic
//
//  Created by Alfari on 23/06/21.
//

import Foundation


protocol StoreViewDelegate: BaseDelegate {
    func onShowData(storeList: [ResponseGetStore])
    func onFailed(msg: String)
}

//
//  HomeViewController.swift
//  Brik Magic
//
//  Created by spe on 3/23/21.
//

import Foundation
import UIKit
import SwiftyUserDefaults
import FSPagerView
import Kingfisher


class HomeViewController: BaseViewController<HomeViewPresenter> {
    
    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lblWelcom: UILabel!
    @IBOutlet var viewBackground: UIView!
    
    var refreshControl = UIRefreshControl()
    
    var delegate: DashboardViewDelegate!
    
    @IBOutlet weak var imgCard: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMemberId: UILabel!
    @IBOutlet weak var lblSince: UILabel!
    @IBOutlet weak var btnUpgrade: UIView!
    
    @IBOutlet weak var viewMemberAktif: UIView!
    @IBOutlet weak var viewMemberNonAktif: UIView!
    
    var dataBannersList: [DetailsDataBanners] = []
    var newsList : [ResponseGetNews] = []
    var memberDetails: DataMemberDetails!
    
    @IBOutlet weak var pageViewNews: FSPagerView!{
        didSet {
            self.pageViewNews.register(UINib(nibName: "NewsBannerPager", bundle: nil),  forCellWithReuseIdentifier: "newsBannerCell")
            self.pageViewNews.itemSize = FSPagerView.automaticSize
            self.pageViewNews.dataSource = self
            self.pageViewNews.delegate = self
        }
    }
    
    @IBOutlet weak var pagerBanners: FSPagerView!{
        didSet {
            self.pagerBanners.register(UINib(nibName: "BannerPagerCell", bundle: nil),  forCellWithReuseIdentifier: "bannerCell")
            self.pagerBanners.itemSize = FSPagerView.automaticSize
            self.pagerBanners.dataSource = self
            self.pagerBanners.delegate = self
        }
    }
    
    @IBOutlet weak var pagerC: FSPageControl!
    
    class func instance(delegate: DashboardViewDelegate)->HomeViewController {
        let vc = HomeViewController.init(nibName: "HomeViewController", bundle: nil)
        vc.delegate = delegate
        return vc
    }
    
    override func viewDidLoad() {
        presenter = HomeViewPresenter(view: self)
        presenter.initBase(view: presenter)
        
        refreshControl.tintColor = UIColor.black
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        
        scrollView.refreshControl = refreshControl
        refreshControl.isEnabled = false
        
        loading()
        presenter.getDataBanner()
        presenter.getDataNews()
        
//        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 1000)
        
        
        let backgroundImage = UIImageView(frame: self.viewBackground.bounds)
        backgroundImage.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        backgroundImage.image = UIImage(named: "bg_dasar.png")
        backgroundImage.backgroundColor = UIColor.clear
        self.viewBackground.insertSubview(backgroundImage, at: 0)
        
        memberDetails = Defaults[.dataBaseMemberDetail]
        if Defaults[.dataBaseMemberDetail] != nil {
            
//            let dateFormatterGet = DateFormatter()
//            dateFormatterGet.dateFormat = "yyyy-MM-dd"
//
//            let dateFormatterPrint = DateFormatter()
//            dateFormatterPrint.dateFormat = "MMMM dd, yyyy"
//
//            let dateDate: NSDate? = dateFormatterGet.date(from: memberDetails.JOIN_DATE) as NSDate?
//            let date = dateFormatterPrint.string(from: dateDate! as Date)
            
            lblWelcom.text = "Welcome, " + memberDetails.FIRST_NAME
            lblName.text = memberDetails.FIRST_NAME + memberDetails.LAST_NAME
            lblMemberId.text = memberDetails.DISPLAY_ID
            lblSince.text = memberDetails.JOIN_DATE
            
            if memberDetails.UPGRADE == false && memberDetails.VIB_MEMBER == false {
                viewMemberNonAktif.alpha = 1
                viewMemberAktif.alpha = 0
            } else {
                viewMemberNonAktif.alpha = 0
                viewMemberAktif.alpha = 1
            }
        }
        
        setupLbl() 
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 900)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        isBackSwipeEnable(visible: true)
    }
    
    
    @objc func didPullToRefresh() {
        loading()
        presenter.getDataBanner()
        presenter.getDataNews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func isBackSwipeEnable(visible: Bool){
        self.navigationController?.interactivePopGestureRecognizer!.isEnabled = visible
    }
}

extension HomeViewController {
    func setupLbl(){
        btnUpgrade.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doClickUpgrade)))
    }
}

extension HomeViewController {
    
    @objc func doClickUpgrade(){
        showAlertUpdate()
    }
}

extension HomeViewController: HomeViewDelegate {
    
    func onShowDataNews(responseDataNews: [ResponseGetNews]) {
        refreshControl.endRefreshing()
        refreshControl.isEnabled = true
        removeLoading()
        self.newsList = responseDataNews
        self.pageViewNews.reloadData()
    }
    
    func onShowBanner(responseDataBanners: [DetailsDataBanners]) {
        refreshControl.endRefreshing()
        refreshControl.isEnabled = true
        removeLoading()
        self.dataBannersList = responseDataBanners
        self.pagerBanners.reloadData()
    }
    
    func onShowDataMember() {
        refreshControl.endRefreshing()
        refreshControl.isEnabled = true
        removeLoading()
    }
    
    func onFailed(msg: String) {
        refreshControl.endRefreshing()
        refreshControl.isEnabled = true
        removeLoading()
        showAlertError(info: "Error", message: msg.localized())
    }
}

extension HomeViewController: FSPagerViewDataSource, FSPagerViewDelegate {
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        
        switch pagerView {
        case pagerBanners:
            //self.pagerC.numberOfPages = self.dataBannersList.count
            return self.dataBannersList.count
        case pageViewNews:
            return self.newsList.count
            
        default:
            return 0
            
        }
    }
    
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        if (pagerView == pagerBanners) {
            let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "bannerCell", at: index) as! BannerCell
            
            cell.imgBanners.sd_setImage(with: URL(string: "http://103.6.53.214:8888/bricksmagic" + dataBannersList[index].PATH ), placeholderImage: UIImage(named: "ic_no_images"))
            
            cell.imgBanners?.clipsToBounds = true
            
            return cell
            
        } else if (pagerView == pageViewNews){
            let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "newsBannerCell", at: index) as! NewsBannerPager
            
            cell.imgNewsBanner.sd_setImage(with: URL(string: "http://103.6.53.214:8888/bricksmagic" + newsList[index].IMAGE ), placeholderImage: UIImage(named: "ic_no_images"))
            
            cell.imgNewsBanner?.clipsToBounds = true
            cell.lblNameBanner.text = newsList[index].TITLE
            cell.lblDescBanner.text = newsList[index].SUB_TITLE
            
            return cell
            
        }else{
            
            return FSPagerViewCell()
            
        }
    }
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        var idProduct = ""
        if(pagerView == pageViewNews){
            idProduct = newsList[index].REC_ID
            let controller = DetailNewsViewController.instance(idProduct: idProduct)
            controller.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
}

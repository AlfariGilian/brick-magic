//
//  HomeViewPresenter.swift
//  Brik Magic
//
//  Created by Alfari on 23/06/21.
//

import Foundation
import SwiftyUserDefaults

class HomeViewPresenter : BasePresenter<HomeViewDelegate>, BaseProtocol {
    
    func doMemberDetail() {
        let responseUserLogin = Defaults[.responseUserLogin]
        
        formData = [
            Tags.ID : "\(responseUserLogin?.MEMBER_ID ?? "")"
        ]
        
        method = ApiProvider.getMemberDetail(data: formData)
        requestId = 0
        Api<BaseMember>.baseRequest(requestId : requestId, method : method, viewProt: view2)
    }
    
    
    func getDataBanner() {
        formData = [:]
        method = ApiProvider.getDataBanners(data: formData)
        requestId = 1
        Api<BaseDataBanners>.baseRequest(requestId : requestId, method : method, viewProt: view2)
    }
    
    func getDataNews() {
        requestId = 2
        formData = [:]
        let method = ApiProvider.getDataNews(data: formData)
        Api<BaseNews>.baseRequest(requestId : requestId, method : method, viewProt: self)
    }
    
    
    func onSuccessRequest(requestId: Int, rawdata: String) {
        switch requestId {
        case 0:
            let objectData = try? JSONDecoder().decode(BaseMember.self, from: rawdata.data(using: .utf8)!)
            break
            
            
        case 1:
            let objectData = try? JSONDecoder().decode(BaseDataBanners.self, from: rawdata.data(using: .utf8)!)
            view.onShowBanner(responseDataBanners: (objectData?.data)!)
            break
            
        case 2:
            let objectData = try? JSONDecoder().decode( BaseNews.self, from: rawdata.data(using: .utf8)!)
            //            Defaults[.newsList] = objectData!.data
            view.onShowDataNews(responseDataNews: objectData!.data)
            break
        default:
            break
        }
    }
    
    func onFailedRequest(status: String, requestId: Int, message: String) {
        switch requestId {
        default:
            view.onFailed(msg: message)
            break
        }
    }
}

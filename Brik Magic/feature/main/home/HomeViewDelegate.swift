//
//  HomeViewDelegate.swift
//  Brik Magic
//
//  Created by Alfari on 23/06/21.
//

import Foundation

protocol HomeViewDelegate: BaseDelegate {
    
    func onShowDataNews(responseDataNews: [ResponseGetNews])
    
    func onShowBanner(responseDataBanners: [DetailsDataBanners])
    
    func onShowDataMember()
    
    func onFailed(msg: String)
}

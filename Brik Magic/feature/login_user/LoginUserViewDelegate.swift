//
//  LoginUserViewDelegate.swift
//  Brik Magic
//
//  Created by Alfari on 21/06/21.
//

import Foundation

protocol LoginUserViewDelegate: BaseDelegate {
    
    func onFailedRequest(message: String)
    
    func successLogin()
    
    func onSuccessAddBg()
    
    func onFailedAddBg()
}

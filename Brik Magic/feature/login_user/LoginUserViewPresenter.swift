//
//  LoginUserViewPresenter.swift
//  Brik Magic
//
//  Created by Alfari on 21/06/21.
//

import Foundation
import SwiftyUserDefaults

class LoginUserViewPresenter: BasePresenter<LoginUserViewDelegate>, BaseProtocol {
    
    
    func doLoginuser(tlp: String, password: String) {
        formData = [
            Tags.PHONE : tlp,
            Tags.PASSWORD : password
        ]
        
        method = ApiProvider.login(data: formData)
        requestId = 1
        Api<BaseResponseUserLogin>.baseRequest(requestId : requestId, method : method, viewProt: view2)
    }
    
    func doMemberDetail() {
        let responseUserLogin = Defaults[.responseUserLogin]
        
        formData = [
            Tags.ID : "\(responseUserLogin?.MEMBER_ID ?? "")"
        ]
        
        method = ApiProvider.getMemberDetail(data: formData)
        requestId = 2
        Api<BaseMember>.baseRequest(requestId : requestId, method : method, viewProt: view2)
    }
    
    func doMemberCardBg() {
        formData = [:]
        method = ApiProvider.getMemberCard(data: formData)
        requestId = 0
        Api<BaseBackground>.baseRequest(requestId : requestId, method : method, viewProt: view2)
    }
    
    
    func onSuccessRequest(requestId: Int, rawdata: String) {
        switch requestId {
        case 0:
            let objectData2 = try? JSONDecoder().decode( BaseBackground.self, from: rawdata.data(using: .utf8)!)
            Defaults[.bgCard] = objectData2?.data
            if Defaults[.bgCard] != nil {
                view.onSuccessAddBg()
            } else {
                view.onFailedAddBg()
            }
            
            break
            
        case 1:
            let objectData = try? JSONDecoder().decode( BaseResponseUserLogin.self, from: rawdata.data(using: .utf8)!)
            
            if objectData?.message == "correct password" {
                Defaults[.responseUserLogin] = objectData?.data
                if(Defaults[.responseUserLogin] != nil){
                    doMemberDetail()
                }
            } else {
                view.onFailedRequest(message: (objectData?.message)!)
            }
            break
            
        case 2:
            let objectData1 = try? JSONDecoder().decode(BaseMember.self, from: rawdata.data(using: .utf8)!)
            Defaults[.dataBaseMemberDetail] = objectData1?.data
            if(Defaults[.dataBaseMemberDetail] != nil){
                view.successLogin()
            } else {
                view.onFailedRequest(message: "Failed")
            }
            break
            
        default:
            break
            
        }
    }
    
    func onFailedRequest(status: String, requestId: Int, message: String) {
        switch requestId {
        default:
            view.onFailedRequest(message: message)
            break
        }
    }
    
    
}

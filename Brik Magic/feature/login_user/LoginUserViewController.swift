//
//  LoginUserViewController.swift
//  Brik Magic
//
//  Created by spe on 4/8/21.
//

import Foundation
import UIKit
import SwiftyUserDefaults

class LoginUserViewController: BaseViewController<LoginUserViewPresenter> {
    
    @IBOutlet weak var viewPhoneNumber: UIView!
    
    @IBOutlet weak var txtPhoneNumber: UITextField!
    
    @IBOutlet weak var txtPassword: CustomTextField!
    
    @IBOutlet weak var viewPassword: UIView!
    
    @IBOutlet weak var viewBg: UIView!
    
    @IBOutlet weak var btnLogin: UIButton!
    
    @IBOutlet weak var lblChangeNumber: UILabel!
    
    @IBOutlet weak var lblResetPassword: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var mobilePhone = ""
    
    
    class func instance(mobilePhone: String)->LoginUserViewController {
        let vc = LoginUserViewController.init(nibName: "LoginUserViewController", bundle: nil)
        vc.mobilePhone = mobilePhone
        return vc
    }
    
    override func viewDidLoad() {
        
        presenter = LoginUserViewPresenter(view: self)
        presenter.initBase(view: presenter)
//      loading()
        presenter.doMemberCardBg()
        
        let backgroundImage = UIImageView(frame: self.viewBg.bounds)
        backgroundImage.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        backgroundImage.image = UIImage(named: "bg_frame.png")
        backgroundImage.backgroundColor = UIColor.clear
        self.viewBg.insertSubview(backgroundImage, at: 0)
        
        btnLogin.layer.cornerRadius = 10
        
        txtPhoneNumber.layer.cornerRadius = 10
        txtPhoneNumber.layer.backgroundColor = UIColor.white.cgColor
        txtPhoneNumber.layer.borderColor = UIColor.white.cgColor
        txtPhoneNumber.setLeftPaddingPoints(15)
        txtPhoneNumber.text = mobilePhone
        txtPhoneNumber.isEnabled = false
        
        
        txtPassword.layer.cornerRadius = 10
        txtPassword.layer.backgroundColor = UIColor.white.cgColor
        txtPassword.layer.borderColor = UIColor.white.cgColor
        txtPassword.setLeftPaddingPoints(15)
      txtPassword.text = "Password01"
//        txtPassword.text = "123456"
        
        viewPhoneNumber.layer.cornerRadius = 10
        viewPassword.layer.cornerRadius = 10
        
        setupLbl()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 700)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
}

extension LoginUserViewController {
    
    func setupLbl(){
        btnLogin.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doClickLoginUser)))
    }
}

extension LoginUserViewController {
    
    @objc func doClickLoginUser(){
        if txtPassword.text!.isEmpty{
            showAlertError(info: "Error", message: "Please fill in Your password.".localized())
            return
        }
        
        loading()
        presenter.doLoginuser(tlp: txtPhoneNumber.text!, password: txtPassword.text!)        
    }
}

extension LoginUserViewController: LoginUserViewDelegate {
    
    func onSuccessAddBg() {
//        removeLoading()
    }
    
    func onFailedAddBg() {
//        removeLoading()
    }
    
    
    func successLogin() {
        removeLoading()
        let vc = DashboardViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func onFailedRequest(message: String) {
        removeLoading()
        showAlertError(info: "Error", message: message.localized())
    }
    
}

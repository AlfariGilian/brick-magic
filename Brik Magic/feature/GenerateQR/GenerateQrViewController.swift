//
//  GenerateQrViewController.swift
//  Brik Magic
//
//  Created by Alfari on 25/06/21.
//

import Foundation
import UIKit
import SwiftyUserDefaults


class GenerateQrViewController: BaseViewController<GenerateQrViewPresenter> {
    
    @IBOutlet weak var lblDisplayId: UILabel!
    
    @IBOutlet weak var imgQr: UIImageView!
    
    @IBOutlet weak var viewMyPointHistory: UIView!
    
    @IBOutlet weak var viewHowToWork: UIView!
    
    @IBOutlet weak var imgBack: UIImageView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var lblIdVid: UILabel!
    
    var memberDetails: DataMemberDetails!
    
    var delegate: DashboardViewDelegate!
    
    
//    class func instance(delegate: DashboardViewDelegate)->GenerateQrViewController {
//        let vc = GenerateQrViewController.init(nibName: "GenerateQrViewController", bundle: nil)
//        vc.delegate = delegate
//        return vc
//    }
    
    class func instance()->GenerateQrViewController {
        let vc = GenerateQrViewController.init(nibName: "GenerateQrViewController", bundle: nil)
//        vc.delegate = delegate
        return vc
    }
    
    override func viewDidLoad() {
        presenter = GenerateQrViewPresenter(view: self)
        presenter.initBase(view: presenter)
        
        memberDetails = Defaults[.dataBaseMemberDetail]
        
        lblIdVid.text = memberDetails.DISPLAY_ID
        
        viewMyPointHistory.layer.cornerRadius = 10
        viewHowToWork.layer.cornerRadius = 10
        
        let backTap = UITapGestureRecognizer(target: self, action: #selector(dismissVc))
        imgBack.isUserInteractionEnabled = true
        imgBack.addGestureRecognizer(backTap)
        
        setupView()
        succesShowQr()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 700)
    }
    
    @objc func dismissVc(){
        let vc = DashboardViewController()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func succesShowQr(){
        
        let dataQr = memberDetails.DISPLAY_ID.data(using: String.Encoding.ascii)
        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return }
        
        qrFilter.setValue(dataQr, forKey: "inputMessage")
        
        guard let qrImage = qrFilter.outputImage else { return }
        
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let scaledQrImage = qrImage.transformed(by: transform)
        let context = CIContext()
        guard let cgImage = context.createCGImage(scaledQrImage, from: scaledQrImage.extent) else { return }
        let processedImage = UIImage(cgImage: cgImage)
        imgQr.image = processedImage
    }
    
}

extension GenerateQrViewController {
    
    func setupView(){
        viewMyPointHistory.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doClickPointHistory)))
        
        viewHowToWork.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doClickHowToWork)))
    }
    
    @objc func doClickPointHistory(){
        let vc = PointHistoryViewController.instance()
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func doClickHowToWork(){
        print("Bisa Di Klik")
    }
}

extension GenerateQrViewController: GenerateQrViewDelegate{
    
}


//
//  GenerateQrViewPresenter.swift
//  Brik Magic
//
//  Created by Alfari on 25/06/21.
//

import Foundation
import SwiftyUserDefaults

class GenerateQrViewPresenter: BasePresenter<GenerateQrViewDelegate>, BaseProtocol {
    
    
    func doGenerateQr() {
        
    }
    
    func onSuccessRequest(requestId: Int, rawdata: String) {
        
    }
    
    func onFailedRequest(status: String, requestId: Int, message: String) {
        
    }
}


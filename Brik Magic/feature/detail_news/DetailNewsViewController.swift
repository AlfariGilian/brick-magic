//
//  DetailNewsViewController.swift
//  Brik Magic
//
//  Created by spe on 3/23/21.
//

import Foundation
import UIKit
import Localize_Swift
import SwiftyUserDefaults
import SDWebImage

class DetailNewsViewController : BaseViewController<DetailNewsViewPresenter> {
    
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var imgNews: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblDesc1: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var idProduct: String = ""
    
    class func instance(idProduct: String)->DetailNewsViewController {
        let vc = DetailNewsViewController.init(nibName: "DetailnewsViewController", bundle: nil)
        vc.idProduct = idProduct
        return vc
    }
    
    override func viewDidLoad() {
        presenter = DetailNewsViewPresenter(view: self)
        presenter.initBase(view: presenter)
        
        loading()
        presenter.getDetailNews(id: idProduct)
        
        let backTap = UITapGestureRecognizer(target: self, action: #selector(dismissVc))
        imgBack.isUserInteractionEnabled = true
        imgBack.addGestureRecognizer(backTap)
        
        lblTitle.text = ""
        lblDesc.text = ""
        lblDesc1.text = ""
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        scrollView.contentSize = CGSize(width: UIScreen.main.bounds.width, height: 1000)
    }
    
    @objc func dismissVc(){
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
}


extension DetailNewsViewController: DetailNewsViewDelegate {
    
    func onShowDataDetailNews(responseDetailNews: [ResponseDetailNews]) {
        
        removeLoading()
        
        lblTitle.text = responseDetailNews[0].TITLE
        lblDesc.text = responseDetailNews[0].SUB_TITLE
        
        let attrStr = try! NSAttributedString(
            data: responseDetailNews[0].DETAIL.data(using: String.Encoding.unicode, allowLossyConversion: true)!,
            options:[NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
        lblDesc1.attributedText = attrStr
        
        imgNews.sd_setImage(with: URL(string: "http://103.6.53.214:8888/bricksmagic" + responseDetailNews[0].IMAGE), placeholderImage: UIImage(named: "ic_no_images"))
    }
    
    func onFailedData(msg: String) {
        removeLoading()
    }
}

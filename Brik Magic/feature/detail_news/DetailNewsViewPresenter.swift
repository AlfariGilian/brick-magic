//
//  DetailNewsViewPresenter.swift
//  Brik Magic
//
//  Created by Alfari on 23/06/21.
//

import Foundation
import SwiftyUserDefaults


class DetailNewsViewPresenter: BasePresenter<DetailNewsViewDelegate>, BaseProtocol {
    
    
    func getDetailNews(id: String){
        requestId = 0
        formData = [Tags.ID : id]
        
        let method = ApiProvider.getDetailNews(data: formData)
        Api<BaseDetailNews>.baseRequest(requestId : requestId, method : method, viewProt: self)
    }
    
    func onSuccessRequest(requestId: Int, rawdata: String) {
        switch requestId {
        case 0:
            let objectData = try? JSONDecoder().decode( BaseDetailNews.self, from: rawdata.data(using: .utf8)!)
            
            view.onShowDataDetailNews(responseDetailNews: objectData!.data)
            break
            
        default:
            break
        }
    }
    
    func onFailedRequest(status: String, requestId: Int, message: String) {
        switch requestId {
        default:
            view.onFailedData(msg: message)
            break
        }
    }
}

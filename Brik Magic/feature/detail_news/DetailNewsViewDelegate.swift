//
//  DetailNewsViewDelegate.swift
//  Brik Magic
//
//  Created by Alfari on 23/06/21.
//

import Foundation


protocol DetailNewsViewDelegate: BaseDelegate  {
    
    func onShowDataDetailNews(responseDetailNews: [ResponseDetailNews])
    func onFailedData(msg: String)
}

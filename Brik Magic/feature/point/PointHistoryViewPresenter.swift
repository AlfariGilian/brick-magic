//
//  PointHistoryViewPresenter.swift
//  Brik Magic
//
//  Created by Alfari on 23/06/21.
//

import Foundation
import SwiftyUserDefaults


class PointHistoryViewPresenter: BasePresenter<PointHistoryViewDelegate>, BaseProtocol {
    
    func getDataPoint() {
        let responseUserLogin = Defaults[.responseUserLogin]
        
        requestId = 0
        formData = [
//            Tags.MEMBER_ID : "10cebf41bfd3b28da6ec5a48fde8357aa1b84192"
            Tags.ID : (responseUserLogin?.MEMBER_ID)!
        ]
        
        let method = ApiProvider.getDataPointHistory(data: formData)
        Api<BasePointHistory>.baseRequest(requestId : requestId, method : method, viewProt: self)
    }
    
    
    func onSuccessRequest(requestId: Int, rawdata: String) {
        switch requestId {
        case 0:
            let objectData = try? JSONDecoder().decode( BasePointHistory.self, from: rawdata.data(using: .utf8)!)
            
            view.onShowData(responsePointList: objectData!.data, responseDetailTrans: (objectData?.data.trans_detail)!)
            
            break
            
        default:
            break
        }
    }
    
    func onFailedRequest(status: String, requestId: Int, message: String) {
        switch requestId {
            
        default:
            view.onFailed(msg: message)
            break
        }
        
    }
}

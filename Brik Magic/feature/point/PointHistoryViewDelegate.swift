//
//  PointHistoryViewDelegate.swift
//  Brik Magic
//
//  Created by Alfari on 23/06/21.
//

import Foundation


protocol PointHistoryViewDelegate: BaseDelegate {
    
    func onShowData(responsePointList: ResponsePointHistory?, responseDetailTrans: [ResponseDetailTrans])
    
    func onFailed(msg: String)
}

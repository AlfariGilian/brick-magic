//
//  PointHistoryViewController.swift
//  Brik Magic
//
//  Created by spe on 4/13/21.
//

import Foundation
import UIKit
import SwiftyUserDefaults


class PointHistoryViewController: BaseViewController<PointHistoryViewPresenter> {
    
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet weak var lblPoint: UILabel!
    @IBOutlet weak var tbPointHistory: UITableView!
    
    var responsePointHistory: ResponsePointHistory?
    var responseDetailHistory: [ResponseDetailTrans] = []
    
    
    class func instance()->PointHistoryViewController {
        let vc = PointHistoryViewController.init(nibName: "PointHistoryViewController", bundle: nil)
        
        return vc
    }
    
    override func viewDidLoad() {
        presenter = PointHistoryViewPresenter(view: self)
        presenter.initBase(view: presenter)
        
        loading()
        presenter.getDataPoint()
        
        tbPointHistory.dataSource = self
        tbPointHistory.delegate = self
        tbPointHistory.register(UINib(nibName: "PointHistoryViewCell", bundle: nil), forCellReuseIdentifier: "tbPointHistory")
        
        let backTap = UITapGestureRecognizer(target: self, action: #selector(dismissVc))
        imgBack.isUserInteractionEnabled = true
        imgBack.addGestureRecognizer(backTap)
    }
    
    @objc func dismissVc(){
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
}

extension PointHistoryViewController: PointHistoryViewDelegate {
    
    func onShowData(responsePointList: ResponsePointHistory?, responseDetailTrans: [ResponseDetailTrans] ) {
        
        removeLoading()
        self.responseDetailHistory = responseDetailTrans
        tbPointHistory.reloadData()
    }
    
    func onFailed(msg: String) {
        removeLoading()
    }
}

extension PointHistoryViewController: UITableViewDelegate, UITableViewDataSource , UITextFieldDelegate {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return responseDetailHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: PointHistoryViewCell = (tableView.dequeueReusableCell(withIdentifier: "tbPointHistory") as? PointHistoryViewCell)!
        
        cell.lblCode.text = responseDetailHistory[indexPath.row].REF_NO
        cell.lblProductTitle.text = responseDetailHistory[indexPath.row].NAME
        cell.lblRedeem.text = responseDetailHistory[indexPath.row].MEMBER_ID
        cell.lblTgl.text = responseDetailHistory[indexPath.row].TRANS_DATE
        cell.lblPoint.text = responseDetailHistory[indexPath.row].POINT
        
        return cell
    }
    
}

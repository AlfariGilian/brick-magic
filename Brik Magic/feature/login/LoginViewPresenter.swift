//
//  LoginViewPresenter.swift
//  Brik Magic
//
//  Created by Alfari on 17/05/21.
//

import Foundation
import SwiftyUserDefaults

class LoginViewPresenter: BasePresenter<LoginViewDelegate>, BaseProtocol {
    
    
    func doLogin(tlp: String){
        requestId = 0
        formData = [
            Tags.PHONE : tlp
        ]
        
        let method = ApiProvider.checkingPhoneNumber(data: formData)
        Api<BaseDataLogin>.baseRequest(requestId : requestId, method : method, viewProt: view2)
    }
    
    func onSuccessRequest(requestId: Int, rawdata: String) {
        switch requestId {
        case 0:
            
            let objectData = try? JSONDecoder().decode( BaseDataLogin.self, from: rawdata.data(using: .utf8)!)
            view.successLogin(message: objectData?.message ?? "")
            break
            
        default:
            break
            
        }
        
    }
    
    func onFailedRequest(status: String, requestId: Int, message: String) {
        switch requestId {
        default:
            view.onFailedRequest(message: message)
            break
        }
    }
}

//
//  LoginViewController.swift
//  Brik Magic
//
//  Created by spe on 3/23/21.
//

import Foundation
import UIKit
import GoogleMaps

class LoginViewController: BaseViewController<LoginViewPresenter>, CLLocationManagerDelegate{
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtArea: UITextField!
    @IBOutlet weak var viewTextfield: UIView!
    @IBOutlet weak var imgLogo: UIImageView!
    
    let locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    var mobilePhone: String  = ""
    var nominal = ""
    
    private var coordinate = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    
    class func instance()->LoginViewController {
        let vc = LoginViewController.init(nibName: "LoginViewController", bundle: nil)
        return vc
    }
    
    override func viewDidLoad() {
        presenter = LoginViewPresenter(view: self)
        presenter.initBase(view: presenter)
        
        let backgroundImage = UIImageView(frame: self.viewBackground.bounds)
        backgroundImage.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        backgroundImage.image = UIImage(named: "bg_frame.png")
        backgroundImage.backgroundColor = UIColor.clear
        self.viewBackground.insertSubview(backgroundImage, at: 0)
        
        let backgroundImage1 = UIImageView(frame: self.imgLogo.bounds)
        backgroundImage1.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        backgroundImage1.image = UIImage(named: "logo_apps.png")
        backgroundImage1.backgroundColor = UIColor.clear
        self.imgLogo.insertSubview(backgroundImage1, at: 0)
        
        btnLogin.layer.cornerRadius = 10
        
        txtPhoneNumber.layer.backgroundColor = UIColor.white.cgColor
        txtPhoneNumber.layer.borderColor = UIColor.white.cgColor
        txtPhoneNumber.setLeftPaddingPoints(15)
        txtPhoneNumber.text = "1128997756"
        //        txtPhoneNumber.text = "8543218734"
        
        txtArea.layer.backgroundColor = UIColor.lightGray.cgColor
        txtArea.layer.borderColor = UIColor.white.cgColor
        
        viewTextfield.layer.cornerRadius = 10
        
        txtPhoneNumber.roundCorners(corners: [.bottomRight, .topRight], radius: 10.0)
        
        txtArea.roundCorners(corners: [.topLeft, .bottomLeft], radius: 10.0)
        
        
        setupLbl()
        setupTextField()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }
    
    func enableLocationServices() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            // Request when-in-use authorization initially
            //            locationManager.requestWhenInUseAuthorization()
            print("not determined")
            break
            
        case .restricted, .denied:
            // Disable location features
            //            disableMyLocationBasedFeatures()
            print("denied")
            break
            
        case .authorizedWhenInUse:
            // Enable basic location features
            //            enableMyWhenInUseFeatures()
            currentLocation = locationManager.location
            
            print("when in use")
            break
            
        case .authorizedAlways:
            // Enable any of your app's location features
            //            enableMyAlwaysFeatures()
            currentLocation = locationManager.location
            print("always")
            break
        }
    }
    
    func escalateLocationServiceAuthorization() {
        // Escalate only when the authorization is set to when-in-use
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.requestAlwaysAuthorization()
        }
    }
    
    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {   switch status {
    case .restricted, .denied:
        // Disable your app's location features
        //        disableMyLocationBasedFeatures()
        print("denied")
        break
        
    case .authorizedWhenInUse:
        // Enable only your app's when-in-use features.
        //        enableMyWhenInUseFeatures()
        print("when in use")
        currentLocation = locationManager.location
        break
        
    case .authorizedAlways:
        // Enable any of your app's location services.
        //        enableMyAlwaysFeatures()
        print("always")
        currentLocation = locationManager.location
        break
        
    case .notDetermined:
        break
        }
    }
    
}

extension UITextField{
    
    func setLeftImage(imageName:String) {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        imageView.image = UIImage(named: imageName)
        self.leftView = imageView;
        self.leftViewMode = .always
    }
    
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

extension UIView {
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}

extension LoginViewController {
    
    func setupLbl(){
        btnLogin.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doClickCheckUser)))
    }
    
    func setupTextField(){
        txtPhoneNumber.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
}

extension LoginViewController {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        nominal = txtPhoneNumber.text!
        if (Int(nominal) == 0) {
            showAlertError(info: "Error", message: "Cannot be 0 (zero)".localized())
            txtPhoneNumber.text! = ""
            return
        }
    }
    
    @objc func doClickCheckUser() {
        if txtPhoneNumber.text!.isEmpty{
            showAlertError(info: "Error", message: "Phone number can't be empty".localized())
            return
        }
        
        loading()
        mobilePhone = "60"+txtPhoneNumber.text!
        presenter.doLogin(tlp: mobilePhone)
        
    }
}

extension LoginViewController: LoginViewDelegate {
    
    func onFailedRequest(message: String) {
        removeLoading()
        showAlertError(info: "Error", message: message.localized())
    }
    
    func successLogin(message: String){
        removeLoading()
        if message == "user found"{
            
            //            let viewController = (UIStoryboard.init(name: "MainDashBoard", bundle: Bundle.main).instantiateViewController(withIdentifier: "MainDashBoard") as? MainDashBoard)!
            //            navigationController?.pushViewController(viewController, animated: true)
            
            let vc = LoginUserViewController.instance(mobilePhone: mobilePhone)
            navigationController?.pushViewController(vc, animated: true)
            
        } else if message == "no data found" {
            
            //            let vc = OtpViewController.instance(mobilePhone: mobilePhone)
            //            navigationController?.pushViewController(vc, animated: true)
            
            let vc = RegisterViewController.instance(mobilePhone: mobilePhone)
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}



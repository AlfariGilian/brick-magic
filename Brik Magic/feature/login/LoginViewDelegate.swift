//
//  LoginViewDelegate.swift
//  Brik Magic
//
//  Created by Alfari on 17/05/21.
//

import Foundation


protocol LoginViewDelegate: BaseDelegate{
    
    func onFailedRequest(message: String)
    
    func successLogin(message: String)
    
}

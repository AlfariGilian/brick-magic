//
//  DetailProfileViewController.swift
//  Brik Magic
//
//  Created by Alfari on 30/06/21.
//

import Foundation
import UIKit
import IQKeyboardManagerSwift
import SwiftyUserDefaults

class DetailProfileViewController: BaseViewController<DetailProfileViewPresenter> {
    
    @IBOutlet weak var imgBack: UIImageView!
    
    @IBOutlet weak var viewBg: UIView!
    
    @IBOutlet var viewBackground: UIView!
    
    
    @IBOutlet weak var viewFirstName: UIView!
    @IBOutlet weak var viewLastName: UIView!
    @IBOutlet weak var viewPhone: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var viewCardNumber: UIView!
    @IBOutlet weak var viewBirthDate: UIView!
    @IBOutlet weak var viewGender: UIView!
    @IBOutlet weak var viewBtnSave: UIView!
    
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtCardNumber: UITextField!
    @IBOutlet weak var txtBod: UITextField!
    @IBOutlet weak var txtGender: UITextField!
    
    @IBOutlet weak var lblButton: UILabel!
    
    var isActive: Bool = false
    
    var memberDetails: DataMemberDetails!
    
    class func instance()->DetailProfileViewController {
        let vc = DetailProfileViewController.init(nibName: "DetailProfileViewController", bundle: nil)
        return vc
    }
    
    
    override func viewDidLoad() {
        presenter = DetailProfileViewPresenter(view: self)
        presenter.initBase(view: presenter)
        
        IQKeyboardManager.shared.shouldResignOnTouchOutside = false
        
        let backgroundImage = UIImageView(frame: self.viewBackground.bounds)
        backgroundImage.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        backgroundImage.image = UIImage(named: "bg_halaman.png")
        backgroundImage.backgroundColor = UIColor.clear
        self.viewBackground.insertSubview(backgroundImage, at: 0)
        
        viewFirstName.layer.cornerRadius = 10
        viewLastName.layer.cornerRadius = 10
        viewPhone.layer.cornerRadius = 10
        viewEmail.layer.cornerRadius = 10
        viewCardNumber.layer.cornerRadius = 10
        viewBirthDate.layer.cornerRadius = 10
        viewGender.layer.cornerRadius = 10
        viewBtnSave.layer.cornerRadius = 10
        
        let backTap = UITapGestureRecognizer(target: self, action: #selector(dismissVc))
        imgBack.isUserInteractionEnabled = true
        imgBack.addGestureRecognizer(backTap)
        
        memberDetails = Defaults[.dataBaseMemberDetail]
        if Defaults[.dataBaseMemberDetail] != nil {
            self.txtFirstName.text = memberDetails.FIRST_NAME
            self.txtLastName.text = memberDetails.LAST_NAME
            self.txtPhone.text = memberDetails.MOBILE_NO
            self.txtEmail.text = memberDetails.EMAIL
            self.txtCardNumber.text = memberDetails.ID_CARD_NO
            self.txtBod.text = memberDetails.BIRTH_DATE
            self.txtGender.text = memberDetails.GENDER
        }
        
        setupLbl()
        setDisable()
    }
    
    override func viewDidAppear(_ animated: Bool) {
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    
    @objc func dismissVc(){
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
}

extension DetailProfileViewController {
    
    func setupLbl(){
        viewBtnSave.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doClickEditProfile)))
    }
    
    func setDisable() {
        self.viewFirstName.backgroundColor = ColorController.greyColorBg()
        self.viewLastName.backgroundColor = ColorController.greyColorBg()
        self.viewPhone.backgroundColor = ColorController.greyColorBg()
        self.viewEmail.backgroundColor = ColorController.greyColorBg()
        self.viewCardNumber.backgroundColor = ColorController.greyColorBg()
        self.viewBirthDate.backgroundColor = ColorController.greyColorBg()
        self.viewGender.backgroundColor = ColorController.greyColorBg()
        
        self.viewBtnSave.backgroundColor = ColorController.blueColorBtn()
        
        self.lblButton.text = "EDIT PROFILE"
        
        self.txtFirstName.isEnabled = false
        self.txtLastName.isEnabled = false
        self.txtPhone.isEnabled = false
        self.txtEmail.isEnabled = false
        self.txtCardNumber.isEnabled = false
        self.txtBod.isEnabled = false
        self.txtGender.isEnabled = false
    }
    
    func setEnable() {
        self.viewFirstName.backgroundColor = UIColor.white
        self.viewLastName.backgroundColor = UIColor.white
        self.viewPhone.backgroundColor = ColorController.greyColorBg()
        self.viewEmail.backgroundColor = UIColor.white
        self.viewCardNumber.backgroundColor = ColorController.greyColorBg()
        self.viewBirthDate.backgroundColor = ColorController.greyColorBg()
        self.viewGender.backgroundColor = ColorController.greyColorBg()
        
        self.viewBtnSave.backgroundColor = ColorController.yelowColorBtn()
        
        self.lblButton.text = "SAVE"
        
        self.txtFirstName.isEnabled = true
        self.txtLastName.isEnabled = true
        self.txtPhone.isEnabled = false
        self.txtEmail.isEnabled = true
        self.txtCardNumber.isEnabled = false
        self.txtBod.isEnabled = false
        self.txtGender.isEnabled = false
        
    }
}

extension DetailProfileViewController {
    
    @objc func doClickEditProfile(){
        if !isActive {
            setEnable()
            isActive = true
            
            
        } else {
            setDisable()
            isActive = false
            
            loading()
            presenter.doRegisterUser(firstName: txtFirstName.text!, lastName: txtLastName.text!, email: txtEmail.text!, idCard: txtCardNumber.text!, dateOfBirth: txtBod.text!)
        }
    }
}

extension DetailProfileViewController : DetailProfileViewDelegate {
    
    func onSuccessRegister(responseDataprofile: DataMemberDetails) {
        removeLoading()
        self.txtFirstName.text = responseDataprofile.FIRST_NAME
        self.txtLastName.text = responseDataprofile.LAST_NAME
        self.txtPhone.text = responseDataprofile.MOBILE_NO
        self.txtEmail.text = responseDataprofile.EMAIL
        self.txtCardNumber.text = responseDataprofile.ID_CARD_NO
        self.txtBod.text = responseDataprofile.BIRTH_DATE
        self.txtGender.text = responseDataprofile.GENDER
    }
    
    func onFailedRequest(message: String) {
        removeLoading()
    }
}

//
//  DetailProfileViewDelegate.swift
//  Brik Magic
//
//  Created by Alfari on 30/06/21.
//

import Foundation

protocol DetailProfileViewDelegate: BaseDelegate {
    
    func onSuccessRegister(responseDataprofile: DataMemberDetails)
    
    func onFailedRequest(message: String)
}

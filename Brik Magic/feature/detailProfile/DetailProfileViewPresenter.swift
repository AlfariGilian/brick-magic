//
//  DetailProfileViewPresenter.swift
//  Brik Magic
//
//  Created by Alfari on 30/06/21.
//

import Foundation
import SwiftyUserDefaults

class DetailProfileViewPresenter: BasePresenter<DetailProfileViewDelegate>, BaseProtocol {
    
    func doRegisterUser(firstName: String, lastName: String, email: String, idCard: String, dateOfBirth: String) {
        
        let responseUserLogin = Defaults[.responseUserLogin]
        
        requestId = 0
        formData = [
            Tags.FIRST_NAME : firstName,
            Tags.LAST_NAME : lastName,
            Tags.EMAIL : email,
            Tags.ID_CARD_NO : idCard,
            Tags.DATE_OF_BIRTH : dateOfBirth,
            Tags.MEMBERID : "\(responseUserLogin?.MEMBER_ID ?? "")"
        ]
        
        let method = ApiProvider.updateProfile(data: formData)
        Api<BaseDataBanners>.baseRequest(requestId : requestId, method : method, viewProt: self)
    }
    
    func doMemberDetail() {
        let responseUserLogin = Defaults[.responseUserLogin]
        
        formData = [
            Tags.ID : "\(responseUserLogin?.MEMBER_ID ?? "")"
        ]
        
        method = ApiProvider.getMemberDetail(data: formData)
        requestId = 1
        Api<BaseMember>.baseRequest(requestId : requestId, method : method, viewProt: view2)
    }
    
    
    func onSuccessRequest(requestId: Int, rawdata: String) {
        switch requestId {
        case 0:
            let objectData1 = try? JSONDecoder().decode(BaseDataBanners.self, from: rawdata.data(using: .utf8)!)
            
            if objectData1?.message == "data updated" {
                doMemberDetail()
            }
            
            break
            
        case 1:
            let objectData1 = try? JSONDecoder().decode(BaseMember.self, from: rawdata.data(using: .utf8)!)
            Defaults[.dataBaseMemberDetail] = objectData1?.data
            if(Defaults[.dataBaseMemberDetail] != nil){
                view.onSuccessRegister(responseDataprofile: (objectData1?.data!)!)
            } else {
                view.onFailedRequest(message: "Failed")
            }
            
            break
        default:
            break
            
        }
    }
    
    func onFailedRequest(status: String, requestId: Int, message: String) {
        switch requestId {
        default:
            view.onFailedRequest(message: message)
            break
        }
    }
}

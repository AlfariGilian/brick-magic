//
//  OtpViewPresenter.swift
//  Brik Magic
//
//  Created by Alfari on 21/06/21.
//

import Foundation
import SwiftyUserDefaults

class OtpViewPresenter: BasePresenter<OtpViewDelegate>, BaseProtocol {
    
    
    func verifyOtp(otp: String, tlp: String) {
        requestId = 0
        formData = [
            Tags.PHONE : tlp,
            Tags.OTP : otp
        ]
        
        let method = ApiProvider.verifyOtp(data: formData)
        Api<BaseDataLogin>.baseRequest(requestId : requestId, method : method, viewProt: self)
    }
    
    func resendOtp(tlp: String) {
        requestId = 0
        formData = [
            Tags.PHONE : tlp
        ]
        
        let method = ApiProvider.resendOtp(data: formData)
        Api<BaseDataLogin>.baseRequest(requestId : requestId, method : method, viewProt: self)
    }
    
    
    
    
    func onSuccessRequest(requestId: Int, rawdata: String) {
        switch requestId {
        case 0:
            
            let objectData = try? JSONDecoder().decode( BaseDataLogin.self, from: rawdata.data(using: .utf8)!)
            
            if objectData?.message == "code verified" {
                view.onSuccessOtp()
            } else {
                view.onFailedOtp(msg: (objectData?.message)!)
            }
            
            break
        default:
            break
        }
    }
    
    func onFailedRequest(status: String, requestId: Int, message: String) {
        switch requestId {
        default:
            view.onFailedOtp(msg: message)
            break
        }
    }
    
    
}


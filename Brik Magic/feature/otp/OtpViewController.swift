//
//  OtpViewController.swift
//  Brik Magic
//
//  Created by spe on 3/23/21.
//

import Foundation
import UIKit
import Localize_Swift

class OtpViewController : BaseViewController<OtpViewPresenter> {
    
    @IBOutlet weak var viewBg: UIView!
    
    @IBOutlet weak var lblPhone: UILabel!
    
    @IBOutlet weak var otpView: VPMOTPView!
    
    @IBOutlet weak var lblOtp: UILabel!
    
    var mobilePhone = ""
    var enteredOtp: String = ""
    
    var timer: Timer?
    var totalTime = 60
    
    
    class func instance(mobilePhone: String)->OtpViewController {
        let vc = OtpViewController.init(nibName: "OtpViewController", bundle: nil)
        vc.mobilePhone = mobilePhone
        return vc
    }
    
    override func viewDidLoad() {
        
        presenter = OtpViewPresenter(view: self)
        presenter.initBase(view: presenter)
        
        let backgroundImage = UIImageView(frame: self.viewBg.bounds)
        backgroundImage.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        backgroundImage.image = UIImage(named: "bg_frame.png")
        backgroundImage.backgroundColor = UIColor.clear
        self.viewBg.insertSubview(backgroundImage, at: 0)
        
        lblPhone.text = mobilePhone
        
        otpView.otpFieldsCount = 4
        otpView.otpFieldBorderWidth = 1
        otpView.delegate = self
        otpView.shouldAllowIntermediateEditing = false
        otpView.otpFieldDisplayType = .circular
        otpView.otpFieldEntrySecureType = true
        otpView.otpFieldSize = 20
        otpView.otpFieldEnteredBackgroundColor = UIColor.black
        otpView.otpFieldDefaultBorderColor = UIColor.white
        otpView.cursorColor = UIColor.clear
        // Create the UI
        otpView.initializeUI()
        
        startOtpTimer()
        
        resend()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func startOtpTimer() {
        self.totalTime = 60
        self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        //        print(self.totalTime)
        self.lblOtp.text! = "Resend Verification Code : " + self.timeFormatted(self.totalTime) // will show timer
        self.lblOtp.isUserInteractionEnabled = false
        
        if totalTime != 0 {
            totalTime -= 1  // decrease counter timer
        } else {
            if let timer = self.timer {
                timer.invalidate()
                self.timer = nil
                self.lblOtp.text = "Resend Verification Code"
                self.lblOtp.isUserInteractionEnabled = true
            }
        }
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        let seconds: Int = totalSeconds % 60
        let minutes: Int = (totalSeconds / 60) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    
    func resend() {
        let resendTap = UITapGestureRecognizer(target: self, action: #selector(resendBtn))
        lblOtp.isUserInteractionEnabled = true
        lblOtp.addGestureRecognizer(resendTap)
    }
    
    @objc func resendBtn(){
        startOtpTimer()
        presenter.resendOtp(tlp: lblPhone.text!)
    }
}

extension OtpViewController: VPMOTPViewDelegate {
    
    func hasEnteredAllOTP(hasEntered: Bool) -> Bool {
        return enteredOtp == "12345"
    }
    
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otpString: String) {
        enteredOtp = otpString
        loading()
        presenter.verifyOtp(otp: otpString, tlp: lblPhone.text!)
    }
}

extension OtpViewController: OtpViewDelegate {
    
    func onSuccessOtp() {
        removeLoading()
        let vc = RegisterViewController.instance(mobilePhone: mobilePhone)
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func onFailedOtp(msg: String) {
        removeLoading()
        showAlertError(info: "Error", message: msg.localized())
    }
}

//
//  OtpViewDelegate.swift
//  Brik Magic
//
//  Created by Alfari on 21/06/21.
//

import Foundation


protocol OtpViewDelegate : BaseDelegate {
 
    func onSuccessOtp()
    
    func onFailedOtp(msg: String)
}

//
//  ChangePasswordViewController.swift
//  Brik Magic
//
//  Created by spe on 4/5/21.
//

import Foundation
import UIKit
import IQKeyboardManagerSwift

class ChangePasswordViewController: BaseViewController<ChangePasswordViewPresenter> {
    
    @IBOutlet weak var viewBg: UIView!
    
    @IBOutlet weak var viewOldPassword: UIView!
    @IBOutlet weak var viewNewPassword: UIView!
    @IBOutlet weak var viewConPass: UIView!
    
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var txtConPass: UITextField!
    
    @IBOutlet weak var btnSave: UIView!
    @IBOutlet weak var imgBack: UIImageView!
    @IBOutlet var viewBackground: UIView!
    
    class func instance()->ChangePasswordViewController {
        let vc = ChangePasswordViewController.init(nibName: "ChangePasswordViewController", bundle: nil)
        return vc
    }
    
    override func viewDidLoad() {
        
        presenter = ChangePasswordViewPresenter(view: self)
        presenter.initBase(view: presenter)
        
        IQKeyboardManager.shared.shouldResignOnTouchOutside = false
        
        let backgroundImage = UIImageView(frame: self.viewBackground.bounds)
        backgroundImage.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        backgroundImage.image = UIImage(named: "bg_halaman.png")
        backgroundImage.backgroundColor = UIColor.clear
        self.viewBackground.insertSubview(backgroundImage, at: 0)
        
        btnSave.layer.cornerRadius = 10
        viewOldPassword.layer.cornerRadius = 10
        viewNewPassword.layer.cornerRadius = 10
        viewConPass.layer.cornerRadius = 10
        
        let backTap = UITapGestureRecognizer(target: self, action: #selector(dismissVc))
        imgBack.isUserInteractionEnabled = true
        imgBack.addGestureRecognizer(backTap)
        
        setupLbl()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.title = ""
    }
    
    
    @objc func dismissVc(){
        self.navigationController?.popViewController(animated: true)
        self.dismiss(animated: true, completion: nil)
    }
}

extension ChangePasswordViewController {
    
    func setupLbl(){
        btnSave.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(doClickCheckUser)))
    }
}

extension ChangePasswordViewController {
    
    @objc func doClickCheckUser() {
        
        if txtOldPassword.text!.isEmpty {
            showAlertError(info: "Error".localized(),message: "Please fill in Your password.".localized())
            return
        }
        
        if txtNewPassword.text!.isEmpty {
            showAlertError(info: "Error".localized(),message: "Please fill in Your old password.".localized())
            return
        }
        
        if txtConPass.text!.isEmpty {
            showAlertError(info: "Error".localized(),message: "Please fill in Your confirmation password.".localized())
            return
        }
        
        if txtNewPassword.text! == txtConPass.text! {
            showAlertError(info: "Error".localized(),message: "Password doesn\'t match.".localized())
            return
        }
        
        loading()
        presenter.changePassword(oldPassword: txtOldPassword.text!, newPass: txtNewPassword.text!)
    }
}


extension ChangePasswordViewController : ChangePasswordViewDelegate {
    
    func onSuccessChange(msg: String) {
        removeLoading()
        showAlertError(info: "Info".localized(),message:"Password renewal has been successful".localized())
    }
    
    func onFailedChange(msg: String) {
        removeLoading()
        showAlertError(info: "Error".localized(),message: msg.localized())
    }
}

//
//  ChangePasswordViewDelegate.swift
//  Brik Magic
//
//  Created by Alfari on 23/06/21.
//

import Foundation

protocol ChangePasswordViewDelegate: BaseDelegate {
    
    func onSuccessChange(msg: String)
    
    func onFailedChange(msg: String)
}

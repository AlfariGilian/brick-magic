//
//  ChangePasswordViewPresenter.swift
//  Brik Magic
//
//  Created by Alfari on 23/06/21.
//

import Foundation
import SwiftyUserDefaults

class ChangePasswordViewPresenter: BasePresenter<ChangePasswordViewDelegate>, BaseProtocol {
    
    
    func changePassword(oldPassword: String, newPass: String) {
        requestId = 0
        formData = [
            Tags.OLD_PASSWORD : oldPassword,
            Tags.NEW_PASSWORD : newPass
        ]
        
        let method = ApiProvider.changePassword(data: formData)
        Api<BaseDataLogin>.baseRequest(requestId : requestId, method : method, viewProt: self)
    }
    
    func onSuccessRequest(requestId: Int, rawdata: String) {
        switch requestId {
               case 0:
                   let objectData = try? JSONDecoder().decode( BaseDataLogin.self, from: rawdata.data(using: .utf8)!)

                   view.onSuccessChange(msg: (objectData?.message)!)
                   break
                   
               default:
                   break
                   
               }
    }
    
    func onFailedRequest(status: String, requestId: Int, message: String) {
        switch requestId {
              default:
                  view.onFailedChange(msg: message)
                  break
              }
    }
}

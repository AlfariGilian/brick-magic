//
//  BasePresenter.swift
//  Brik Magic
//
//  Created by spe on 4/13/21.
//

import Foundation
import SwiftyJSON

protocol PresenterCommonDelegate {
    func getDeviceId() -> String
    func isNetworkAvailable() -> Bool
}

class BasePresenter<T>: PresenterCommonDelegate {
    var formData : [String: Any]!
    var method: ApiProvider!
    var requestId = 12399
    var view: T!
    var view2: BaseProtocol!
    var AUTHENTICATION = 12399
    init(view: T!) {
        self.view = view!
    }
  
    func initBase(view: BaseProtocol!) {
        self.view2 = view!
    }
    
    func isNetworkAvailable() -> Bool {
        return false
    }
    
    func getDeviceId() -> String {
        return ""
    }
    
//    func changePhone(text: String) -> String{
//        var text1 = text
//        let index = text1.index(text1.startIndex, offsetBy: 1)
//        let mySubstring = text1[..<index]
//
//        if(mySubstring == "0"){
//            text1 = "62"+text1.substring(from: 1)
//        }
//        return text1
//    }
    
   
    
    
    func reCall<T>( T: T.Type) where T : Codable{
        Api<T>.baseRequest(requestId : requestId, method : method as! ApiProvider, viewProt: view2)
    }
    
}


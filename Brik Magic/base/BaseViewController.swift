//
//  BaseViewController.swift
//  Brik Magic
//
//  Created by spe on 4/13/21.
//

import Foundation
import IQKeyboardManagerSwift
import NVActivityIndicatorView
import UIKit
import IQKeyboardManagerSwift
import SwiftyUserDefaults

protocol ApplicationStateDelegate {
    func appWillForeground()
    func appWillBackground()
}

class BaseViewController<T:PresenterCommonDelegate>: UIViewController {
    var presenter: T!
    var stateDelegate: ApplicationStateDelegate?
    var window: UIWindow!
    
    
    var activityIndicator:UIActivityIndicatorView!
    var activityIndicat:NVActivityIndicatorView!
    
    var loadingScreenController = LoadingScreenController.instance()
    
    var screenHeight: CGFloat?
    var screenWidth: CGFloat?
    
    var dialogError: DialogErrorMessage!
    var dialogUpdate: DialogUpgradeMember!
    var dialogMessageUpdate: DialogMessageUpdate!
    
    let alertView: UIView = {
        let view = UIView()
        view.backgroundColor = .red
        return view
    }()
    
    let alertLabel: UILabel = {
        let label = UILabel()
        return label
    }()
    
    var alert: UIAlertController!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        screenHeight = UIScreen.main.bounds.height
        screenWidth = UIScreen.main.bounds.width
        
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = UIColor.white
            view.addSubview(statusbarView)
            
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor
                .constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor
                .constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor
                .constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor
                .constraint(equalTo: view.centerXAnchor).isActive = true
            
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = .white
        }
        
    }
    
    
    //
    // by default, this will register an observer to listen foreground/background state
    //
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        registerAppDelegateObserver()
    }
    
    //
    // by default, this will remove an observer to listen foreground/background state
    //
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeAppDelegateObserver()
    }
    
    func userInterActionEnabled(bool: Bool) {
        if bool {
            UIApplication.shared.endIgnoringInteractionEvents()
        } else {
            UIApplication.shared.beginIgnoringInteractionEvents()
        }
    }
    
    @objc  func done(show: Bool, message: String){
        loadingScreenController.updateDone(message: message.localized())
    }
    
    func addActivityIndicator() {
        activityIndicat = NVActivityIndicatorView(frame: self.view.bounds, type: NVActivityIndicatorType.ballTrianglePath, color: ColorController.blueSpe2(), padding: ScreenSizeUtil.screenSize())
        print(ScreenSizeUtil.screenSize())
        activityIndicat.startAnimating()
        activityIndicat.backgroundColor = UIColor(white: 0, alpha: 0)
        self.view.addSubview(activityIndicat)
        userInterActionEnabled(bool: false)
    }
    
    func removeActivityIndicator() {
        if activityIndicat != nil {
            activityIndicat.stopAnimating()
            activityIndicat.removeFromSuperview()
            activityIndicat = nil
        }
        userInterActionEnabled(bool: true)
        
    }
    
    //
    // register observer like appDelegate, but only notify if app in foreground/background
    //
    private func registerAppDelegateObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(appWillEnterForeground(_:)), name: UIApplication.didEnterBackgroundNotification, object: nil )
        
        //            Notification.Name.UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(appWillEnterBackground(_:)), name: UIApplication.willResignActiveNotification , object: nil)
    }
    
    //
    // remove observer appDelegate
    //
    private func removeAppDelegateObserver() {
        NotificationCenter.default.removeObserver(self, name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    @objc private func appWillEnterForeground(_ notification: Notification) {
        stateDelegate?.appWillForeground()
    }
    
    @objc private func appWillEnterBackground(_ notification: Notification) {
        stateDelegate?.appWillBackground()
    }
    
    //
    // showing alert dialog, with default text button
    //
    func showError(title: String?, message: String, positiveText: String = "OK", handler: ((UIAlertAction) -> Void)? = nil) {
        let actionPositive = UIAlertAction(title: positiveText, style: .default, handler: handler)
        let actionController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        actionController.addAction(actionPositive)
        actionController.present(self, animated: true)
    }
    
    func showConfirmationDialog(title: String?, message: String, buttons: [String], handlers: [((UIAlertAction) -> Void)?]) {
        let actionNegative = UIAlertAction(title: buttons[0], style: .cancel, handler: handlers[0])
        let actionPositive = UIAlertAction(title: buttons[1], style: .default, handler: handlers[1])
        let actionController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        actionController.addAction(actionPositive)
        actionController.addAction(actionNegative)
        actionController.present(self, animated: true)
    }
    
    func getDestinationViewController(storyboardName: String, storyboardId: String) -> UIViewController{
        let storyboard = UIStoryboard.init(name: storyboardName, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: storyboardId)
        return viewController
    }
    //
    // dismiss the showing keyboard
    //
    func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    
    func showAlert(message: String){
        showDialog(title: "Error!", message: message)
    }
    
    func showWarning(message: String){
        showDialog(title: "Warning!", message: message)
    }
    
    func showInfo(message: String){
        showDialog(title: "Info!", message: message)
    }
    
    func showDialog(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc  func loading(){
        self.view.frame = UIScreen.main.bounds
        loadingScreenController.view.center = view.center
        addChild(loadingScreenController)
        self.view.addSubview(loadingScreenController.view)
        loadingScreenController.didMove(toParent: self)
        loadingScreenController.updateLoading()
    }
    
    @objc  func removeLoading(){
        //        showNavigation(show: show)
        loadingScreenController.remove()
    }
    
    func finishViewController(){
        var navigationArray = self.navigationController?.viewControllers //To get all UIViewController stack as Array
        navigationArray!.remove(at: (navigationArray?.count)! - 2) // To remove previous UIViewController
        self.navigationController?.viewControllers = navigationArray!
    }
    
    func showDialogSessionExpired(message: String){
        let title = "Error!"
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        self.present(alert, animated: true, completion: nil)
    }
    
}


extension BaseViewController{
    
    func getHeight(centimeter: CGFloat) -> CGFloat {
        return screenHeight! * centimeter / 14.9
    }
    
    func getWidth(centimeter: CGFloat) -> CGFloat {
        return screenWidth! * centimeter / 8.4
    }
}


extension BaseViewController {
    
    func popAlert(title: String, message: String, defaultAction: UIAlertAction?, cancelAction: UIAlertAction?, destructAction: UIAlertAction?){
        
        alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        if defaultAction != nil {
            alert.addAction(defaultAction!)
        }
        if cancelAction != nil {
            alert.addAction(cancelAction!)
        }
        if destructAction != nil {
            alert.addAction(destructAction!)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert2(message: String){
        let statusHeight = UIApplication.shared.statusBarFrame.height
        let navBarHeight = self.navigationController?.navigationBar == nil ? 40 : (self.navigationController?.navigationBar.frame.height)!
        let height = statusHeight + navBarHeight
        window = UIApplication.shared.keyWindow
        alertLabel.textColor = .white
        alertLabel.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold)
        alertLabel.text = message
        alertLabel.numberOfLines = 0
        alertLabel.sizeThatFits(CGSize(width: window.frame.width - getHeight(centimeter: 1.2), height: navBarHeight))
        
        alertView.frame = CGRect(x: 0, y: 0 - height, width: window.frame.width, height: height)
        
        alertView.addSubview(alertLabel)
        //        alertLabel.anchor(alertView.topAnchor, left: alertView.leftAnchor, bottom: nil, right: alertView.rightAnchor, topConstant: statusHeight, leftConstant: self.getHeight(centimeter: 0.6), bottomConstant: 0, rightConstant: self.getHeight(centimeter: 0.6), widthConstant: 0, heightConstant: navBarHeight)
        alertLabel.frame = CGRect(x: alertView.frame.height * 0.2 , y: statusHeight, width: self.view.frame.width - alertView.frame.height * 0.4, height: navBarHeight)
        window.addSubview(alertView)
        
        alertView.backgroundColor = .red
        
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.alertView.frame = CGRect(x: 0, y: 0, width: self.window.frame.width, height: height)
        }, completion: nil)
        
        let when = DispatchTime.now() + 2 // change 1 to desired number of seconds
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.removeAlert()
        }
    }
    
    func setBaseToolbar(toolbarTitle: String){
        
        let font = UIFont(name: "OpenSans-SemiBold", size: 12)
        let textAttributes = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.topItem?.title = " "
        navigationController?.navigationItem.backBarButtonItem?.title = " "
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationController?.navigationBar.backIndicatorImage = UIImage(named: "ic_arrow_back")
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "ic_arrow_back")
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.barTintColor = ColorController.orangeBersama1()
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.shadowImage = UIImage()
        self.title = toolbarTitle
    }
    
    func setHintTextFieldlogin(textField: UITextField, hintText: String){
        textField.attributedPlaceholder =
            NSAttributedString(string: hintText, attributes: [NSAttributedString.Key.foregroundColor: ColorController.textHint3()])
    }
    
    func setHintTextField(textField: UITextField, hintText: String){
        textField.attributedPlaceholder =
            NSAttributedString(string: hintText, attributes: [NSAttributedString.Key.foregroundColor: ColorController.textHint2()])
    }
    
    func borderTextField(textFields: [UITextField]){
        for i in 0..<textFields.count  {
            setTextFieldBorderColor(textField: textFields[i], borderColor: ColorController.borderBersama()!.cgColor, borderWidth: 1.0)
        }
    }
    
    
    func borderView(views: [UIView]){
        for i in 0..<views.count {
            setViewBorderColor(view: views[i], borderColor: ColorController.borderBersama()!.cgColor, borderWidth: 1.0)
        }
    }
    func setViewBorderColor(view: UIView, borderColor: CGColor, borderWidth: CGFloat){
        view.layer.borderColor = borderColor
        
        view.layer.borderWidth = borderWidth
    }
    
    func setTextFieldBorderColor(textField: UITextField, borderColor: CGColor, borderWidth: CGFloat){
        textField.layer.borderColor = borderColor
        
        textField.layer.borderWidth = borderWidth
    }
    
    func setPaddingTextField(textField: UITextField, width: CGFloat){
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 0))
        textField.leftViewMode = .always
    }
    
    func setPaddingLRTextField(textField: UITextField, width: CGFloat){
        textField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 0))
        textField.leftViewMode = .always
        textField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: 0))
        textField.rightViewMode = .always
    }
    
    
    func showDialogDismiss(message: String){
        var title = "Error!"
        var alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                self.navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func showDialogDismissInfo(message: String){
        var title = "Info!"
        var alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                self.navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
                
            }}))
        self.present(alert, animated: true, completion: nil)
    }
    
    func showNavigation(show: Bool){
        self.navigationController?.isNavigationBarHidden = !show
        self.navigationController?.navigationBar.isHidden = !show
        self.navigationController?.setNavigationBarHidden(!show, animated: true)
    }
    
    
    func removeAlert(){
        let statusHeight = UIApplication.shared.statusBarFrame.height
        let navBarHeight = self.navigationController?.navigationBar == nil ? 0 : (self.navigationController?.navigationBar.frame.height)!
        let height = statusHeight + navBarHeight
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn,  animations: {
            self.alertView.frame = CGRect(x: 0, y: 0 - height, width: self.window.frame.width, height: height)
            self.alertLabel.frame = CGRect(x: self.alertView.frame.height * 0.2 , y: 0 - self.alertLabel.frame.height, width: self.view.frame.width - self.alertView.frame.height * 0.4, height: navBarHeight)
        })
        
    }
    
    func showAlertError(info: String, message: String){
        dialogError = DialogErrorMessage.instance(title: info, message: message, isDismiss: "dismiss", viewDelegate: self)
        dialogError.view.center = view.center
        addChild(dialogError)
        view.addSubview(dialogError.view)
        dialogError.didMove(toParent: self)
    }
    
    func showAlertUpdate(){
        dialogUpdate = DialogUpgradeMember.instance(isDismiss: "dismiss", viewDelegate: self)
        dialogUpdate.view.center = view.center
        addChild(dialogUpdate)
        view.addSubview(dialogUpdate.view)
        dialogUpdate.didMove(toParent: self)
    }
    
    func showAlertMessageUpdate(){
        dialogMessageUpdate = DialogMessageUpdate.instance(isDismiss: "dismiss", viewDelegate: self)
        dialogMessageUpdate.view.center = view.center
        addChild(dialogMessageUpdate)
        view.addSubview(dialogMessageUpdate.view)
        dialogMessageUpdate.didMove(toParent: self)
    }
}


extension BaseViewController: DialogErrorDelegate {
    
    func executeError() {
        dialogError.remove()
    }
    
    func executeDismissError() {
        dialogError.remove()
    }
    
    func executeDashboard() {
        dialogError.remove()
    }
}

extension BaseViewController: DialogUpgradeMemberDelegate {
    
//    func onFailedMemberUpgrade(message: String) {
//        removeLoading()
//        dialogUpdate.remove()
//    }
//
//    func successUpdagradeMember() {
//        removeLoading()
//        dialogUpdate.remove()
//        showAlertMessageUpdate()
//    }
    
    func excuteError() {
          dialogUpdate.remove()
        showAlertMessageUpdate()
      }
      
      func excuteDismissError() {
          dialogUpdate.remove()
      }
      
      func excuteDashboard() {
          dialogUpdate.remove()
      }
}

extension BaseViewController: DialogMemberUpdateDelegate {

    func extError() {
        dialogMessageUpdate.remove()
    }

    func extDismissError() {
        dialogMessageUpdate.remove()
    }

    func extDashboard() {
        dialogMessageUpdate.remove()
    }
}

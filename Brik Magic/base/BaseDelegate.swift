//
//  BaseDelegate.swift
//  Brik Magic
//
//  Created by spe on 4/13/21.
//

import Foundation
import UIKit

protocol BaseDelegate {
    func taskDidBegin()
    func taskDidFinish()
    
    func taskSessionDidInvalid()
    func taskDidError(message: String)
}

extension BaseDelegate {
    func taskDidBegin(){}
    func taskDidFinish(){}
    func taskDidError(message: String){}
    func taskSessionDidInvalid(){}
}


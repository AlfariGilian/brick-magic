//
//  BaseProtocol.swift
//  Brik Magic
//
//  Created by spe on 4/13/21.
//

import Foundation

protocol BaseProtocol {
    func onSuccessRequest(requestId: Int, rawdata: String)
    func onFailedRequest(status: String, requestId: Int, message: String)
}

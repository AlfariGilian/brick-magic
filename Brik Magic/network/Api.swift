//
//  Api.swift
//  Brik Magic
//
//  Created by spe on 4/13/21.
//

import Foundation
import Moya
import SwiftyJSON

class Api<T: Codable> {
    
    static func baseRequest( requestId : Int, method:ApiProvider, viewProt : BaseProtocol){
        
        var _ = NetworkManager<ApiProvider>(verbose: true).api().request(method) {
            result in
            switch result {
            case .success(let response):
                
                let  request = try? JSONDecoder().decode(T.self, from: response.data)
                
                if(request != nil){
                    let rawdata = try? String(data: JSONEncoder().encode(request!), encoding: String.Encoding.utf8)
                    
                    viewProt.onSuccessRequest(requestId: requestId, rawdata: rawdata!!)
                    
                } else {
                                        
                    viewProt.onFailedRequest(status: "999",requestId: requestId, message: "Time Out")
                }
                
                break
            case .failure(let err):
                viewProt.onFailedRequest(status: "999", requestId: requestId, message: err.errorDescription!)
                break
            }
        }
        
    }
    
}

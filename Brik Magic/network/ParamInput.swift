//
//  ParamInput.swift
//  Brik Magic
//
//  Created by spe on 4/13/21.
//

import Foundation
import SwiftyJSON

class ParamInput{
    
    var username: String = ""
    var password: String = ""
    
}

class Constant {
    
    static let BASE_URL = "http://103.6.53.214:8888"
    
    static let CHECKING_NUMBER = "/bricksmagic/API/postUserPhone"
    static let GET_NEWS = "/bricksmagic/API/getPostList"
    static let GET_BANNERS = "/bricksmagic/API/getBanner"
    static let REGISTER = "/bricksmagic/API/postRegisterUser"
    static let LOGIN_USER = "/bricksmagic/API/postUserLogin"
    static let GET_STORE = "/bricksmagic/API/getStore"
    static let VERIFY_OTP = "/bricksmagic/API/postVerifyOTP"
    static let RESEND_OTP = "/bricksmagic/API/postResendOTP"
    static let DETAIL_NEWS = "/bricksmagic/API/getPostDetail"
    static let GET_MEMBER_TRANSACTION = "/bricksmagic/API/getMemberTransaction"
    static let UPDATE_MEMBER_PASSWORD = "/bricksmagic/API/updateMemberPassword"
    static let GET_MEMBER_DETAIL = "/bricksmagic/API/getMemberDetail"
    static let GET_MEMBER_CARD = "/bricksmagic/API/getCardbackground"
    static let GET_TERM = "/bricksmagic/API/getTermsandCondition"
    static let UPDATE_MEMBER_PROFILE = "/bricksmagic/API/updateMemberProfile"
    static let UPDATE_MEMBER = "/bricksmagic/API/postUpdateStatus"
    
    // Base Request and Response Tag
    static let DATA = "data"
    static let STATUS = "status"
    static let MESSAGE_ID = "message_id"
    static let MESSAGE_EN = "message_en"
    static let SECRET_KEY = "Secret Key"
    static let MESSAGE = "message"
    static let DEVICE_ID = "device_id"
    static let SK_TAG = "secret_key"
    
    static let PHONE = "phone"
    static let PASSWORD = "password"
    static let PASS = "pass"
    static let FIRST_NAME = "firstName"
    static let LAST_NAME = "lastName"
    static let PHONE_NUMBER = "mobileNo"
    static let EMAIL = "email"
    static let ID_CARD = "ID_NO"
    static let ID_CARD_NO = "idCardNo"
    static let DATE_OF_BIRTH = "dob"
    static let COMFIRM_PASSWORD = "copass"
    static let GENDER = "gender"
    static let MEMBER_ID = "member_id"
    static let MEMBERID = "memberID"
    static let ID = "id"
    static let OLD_PASSWORD = "curPassword"
    static let NEW_PASSWORD = "newPassword"
     static let OTP = "otp"
    
    
    // Checking status code
    static func isSuccessCode(code : Int) -> Bool {
        return (code == 200)
    }
}


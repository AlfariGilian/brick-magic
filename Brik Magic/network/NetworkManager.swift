//
//  NetworkManager.swift
//  Brik Magic
//
//  Created by spe on 4/13/21.
//

import Foundation
import Alamofire
import Moya

public class NetworkManager<T: BaseApi> {
    
    let ERROR_MESSAGE = "Internal Error"
    var isVerbose: Bool!
    
    init(verbose: Bool = true) { isVerbose = verbose }
    
    func api() -> MoyaProvider<T> {
        
        let manager = Manager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: [
                Constant.BASE_URL: .pinPublicKeys(publicKeys: ServerTrustPolicy.publicKeys(), validateCertificateChain: true, validateHost: true)
                ])
        )
        
        let provider = MoyaProvider<T>(manager: manager, plugins: [NetworkLoggerPlugin(verbose: isVerbose)])
        return provider
    }
}

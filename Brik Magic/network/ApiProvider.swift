//
//  ApiProvider.swift
//  Brik Magic
//
//  Created by spe on 4/13/21.
//

import Foundation
import Moya


enum ApiProvider: BaseApi {
    
    case login(data: [String: Any])
    case regist(data: [String: Any])
    case checkingPhoneNumber(data: [String: Any])
    case getOtp(data: [String: Any])
    case getDataNews(data: [String: Any])
    case getDataStore(data: [String: Any])
    case getDataPointHistory(data: [String: Any])
    case getDetailNews(data: [String: Any])
    case changePassword(data: [String: Any])
    case getMemberDetail(data: [String: Any])
    case getMemberCard(data: [String: Any])
    case verifyOtp(data: [String: Any])
    case resendOtp(data: [String: Any])
    case getTerm(data: [String: Any])
    case getDataBanners(data: [String: Any])
    case updateProfile(data: [String: Any])
    case updateMember(data: [String: Any])
}

extension ApiProvider: TargetType{
    
    var baseURL: URL {
        guard let url = URL(string: Tags.BASE_URL) else {
            fatalError("Server Bermasalah")
        }
        switch self {
        default:
            return url
        }
    }
    
    
    var path: String {
        switch self {
            
        case .login:
            return Tags.LOGIN_USER
            
        case .regist:
            return Tags.REGISTER
            
        case .checkingPhoneNumber:
            return Tags.CHECKING_NUMBER
            
        case .getOtp:
            return Tags.VERIFY_OTP
            
        case .getDataNews:
            return Tags.GET_NEWS
            
        case .getDataStore:
            return Tags.GET_STORE
            
        case .getDataPointHistory:
            return Tags.GET_MEMBER_TRANSACTION
            
        case .getDetailNews:
            return Tags.DETAIL_NEWS
            
        case .changePassword:
            return Tags.UPDATE_MEMBER_PASSWORD
            
        case .getMemberDetail:
            return Tags.GET_MEMBER_DETAIL
            
        case .getMemberCard:
            return Tags.GET_MEMBER_CARD
            
        case .verifyOtp:
            return Tags.VERIFY_OTP
            
        case .resendOtp:
            return Tags.RESEND_OTP
            
        case .getTerm:
            return Tags.GET_TERM
            
        case .getDataBanners:
            return Tags.GET_BANNERS
            
        case .updateProfile:
            return Tags.UPDATE_MEMBER_PROFILE
            
        case .updateMember:
            return Tags.UPDATE_MEMBER
        }
    }
    
    var method: Moya.Method {
        switch self {
            
        case .login,.regist,.checkingPhoneNumber:
            return .post
            
        case .getOtp:
            return .get
            
        case .getDataNews:
            return .get
            
        case .getDataStore:
            return .get
            
        case .getDataPointHistory:
            return .post
            
        case .getDetailNews:
            return .get
            
        case .changePassword:
            return .post
            
        case .getMemberDetail:
            return .post
            
        case .getMemberCard:
            return .get
            
        case .verifyOtp:
            return .post
            
        case .resendOtp:
            return .post
            
        case .getTerm:
            return .get
            
        case .getDataBanners:
            return .get
            
        case .updateProfile:
            return .post
            
        case .updateMember:
            return .post
        }
    }
    
    var parameters: [String: Any]? {
        switch self {
            
        case .login(let data),.regist(let data),.checkingPhoneNumber(let data):
            var parameters = [String: Any]()
            for (key, value) in data {
                parameters[key] = value
            }
            return parameters
            
        case .getOtp(let data):
            return data
            
        case .getDataNews(let data):
            return data
            
        case .getDataStore(let data):
            return data
            
        case .getDataPointHistory(let data):
            return data
            
        case .getDetailNews(let data):
            return data
            
        case .changePassword(let data):
            return data
            
        case .getMemberDetail(let data):
            return data
            
        case .getMemberCard(let data):
            return data
            
        case .verifyOtp(let data):
            return data
            
        case .resendOtp(let data):
            return data
            
        case .getTerm(let data):
            return data
            
        case .getDataBanners(let data):
            return data
            
        case .updateProfile(let data):
            return data
            
        case .updateMember(let data):
            return data
        }
    }
    
    
    var parameterEncoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    var task: Task {
        switch self {
            
        case .login(let data),.regist(let data), .checkingPhoneNumber(let data):
            
            var formData = [MultipartFormData]()
            
            for (key, value) in data {
                formData.append(MultipartFormData(provider: .data("\(value)".data(using: .utf8)!), name: key))
            }
            
            return .uploadMultipart(formData)
            
        case .getOtp(let data):
            
            return .requestParameters(parameters: data, encoding: URLEncoding.default)
            
        case .getDataNews(let data):
            return .requestParameters(parameters: data, encoding: URLEncoding.default)
            
        case .getDataStore(let data):
            return .requestParameters(parameters: data, encoding: URLEncoding.default)
            
        case .getDataPointHistory(let data):
            return .requestParameters(parameters: data, encoding: URLEncoding.default)
            
        case .getDetailNews(data: let data):
            return .requestParameters(parameters: data, encoding: URLEncoding.default)
            
        case .changePassword(data: let data):
            return .requestParameters(parameters: data, encoding: URLEncoding.default)
            
        case .getMemberDetail(data: let data):
            return .requestParameters(parameters: data, encoding: URLEncoding.default)
            
        case .getMemberCard(data: let data):
            return .requestParameters(parameters: data, encoding: URLEncoding.default)
            
        case .verifyOtp(data: let data):
            return .requestParameters(parameters: data, encoding: URLEncoding.default)
            
        case .resendOtp(data: let data):
            return .requestParameters(parameters: data, encoding: URLEncoding.default)
            
        case .getTerm(data: let data):
            return .requestParameters(parameters: data, encoding: URLEncoding.default)
            
        case .getDataBanners(data: let data):
            return .requestParameters(parameters: data, encoding: URLEncoding.default)
            
        case .updateProfile(data: let data):
            return .requestParameters(parameters: data, encoding:  JSONEncoding.prettyPrinted)
            
        case .updateMember(data: let data):
            return .requestParameters(parameters: data, encoding:  JSONEncoding.prettyPrinted)
        }
        
    }
    
    
    var headers: [String : String]? {
        switch self {
        case
        .login(data: _),
        .regist(data: _),
        .checkingPhoneNumber(data: _),
        .getOtp(data: _),
        .getDataNews(data: _),
        .getDataStore(data: _),
        .getDataPointHistory(data: _),
        .getDetailNews(data: _),
        .changePassword(data: _),
        .getMemberDetail(data: _),
        .getMemberCard(data: _),
        .verifyOtp(data: _),
        .resendOtp(data: _),
        .getTerm(data: _),
        .getDataBanners(data: _),
        .updateProfile(data: _),
        .updateMember(data: _):
            
            return [ "Authorization": ("Basic YnJpY2tzYWRtaW46MzA4ZDM1NDIxMTcyY2M4MzdmNWFjZWU3Y2VkYTViMTBiNmFkN2ZhYw==")]
        }
    }
}
